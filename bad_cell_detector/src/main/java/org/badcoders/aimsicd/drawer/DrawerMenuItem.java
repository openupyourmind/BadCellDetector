package org.badcoders.aimsicd.drawer;

public class DrawerMenuItem implements NavDrawerItem {

    public static final int ITEM_TYPE = 1;

    private String mLabel;
    private int mId;
    private int mIconId;

    public static DrawerMenuItem create(int pMenuId, String pLabel, int pIconDrawableId) {
        return create(pMenuId, pLabel, pIconDrawableId, true);
    }

    public static DrawerMenuItem create(int pMenuId, String pLabel, int pIconDrawableId,  boolean pIsShowInfoButton) {
        DrawerMenuItem item = new DrawerMenuItem();
        item.setId(pMenuId);
        item.setLabel(pLabel);
        item.setIconId(pIconDrawableId);
        return item;
    }

    @Override
    public int getType() {
        return ITEM_TYPE;
    }

    @Override
    public int getId() {
        return mId;
    }

    @Override
    public void setId(int pId) {
        mId = pId;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String pLabel) {
        mLabel = pLabel;
    }

    public int getIconId() {
        return mIconId;
    }

    public void setIconId(int pIcon) {
        mIconId = pIcon;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
