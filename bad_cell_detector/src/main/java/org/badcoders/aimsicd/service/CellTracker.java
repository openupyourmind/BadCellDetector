package org.badcoders.aimsicd.service;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.widget.Toast;

import org.badcoders.aimsicd.BuildConfig;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.model.Device;
import org.badcoders.aimsicd.utils.DeviceApi17;
import org.badcoders.aimsicd.utils.DeviceApi18;
import org.badcoders.aimsicd.utils.Helpers;
import org.badcoders.aimsicd.utils.MyNotification;
import org.badcoders.aimsicd.utils.NetworkUtil;
import org.badcoders.aimsicd.utils.Status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CellTracker extends PhoneStateListener implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "CellTracker";

    private static Cell mCell;
    public static String OCID_API_KEY = null;
    public static int PHONE_TYPE;
    public static int LAST_DB_BACKUP_VERSION;
    public static long REFRESH_RATE;
    public static final String SILENT_SMS = "SILENT_SMS_DETECTED";

    private static final Device mDevice = new Device();

    private static TelephonyManager tm;
    private SharedPreferences prefs;

    private boolean mMonitoringCell;
    private boolean mTrackingCell;
    private boolean mTrackingFemtocell;
    private boolean mFemtoDetected;
    private boolean mChangedLAC;
    private boolean mCellIdNotInOpenDb;

    private DatabaseAdapter dbHelper;
    private Context mContext;

    public CellTracker(Context context) {
        this.mContext = context;

        // TelephonyManager provides system details
        tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        // Shared Preferences
        prefs = context.getSharedPreferences(AimsicdService.SHARED_PREFERENCES_BASENAME, 0);
        prefs.registerOnSharedPreferenceChangeListener(this);
        loadPreferences();
        updateNotification();

        PHONE_TYPE = tm.getPhoneType();

        dbHelper = new DatabaseAdapter(context);

        mDevice.refreshDeviceInfo(tm);
        mCell = new Cell();
        setCellMonitoring(true); //starts notification
    }

    @Override
    public void onCellLocationChanged(CellLocation location) {
        compareLac(location);
        refreshDevice();

        switch(mDevice.getPhoneID()) {
            case TelephonyManager.PHONE_TYPE_GSM:
                GsmCellLocation gsmCellLocation = (GsmCellLocation) location;
                if(gsmCellLocation != null) {
                    mCell.setCellId(gsmCellLocation.getCid());
                    mCell.setLAC(gsmCellLocation.getLac());
                    mCell.setMCC(Helpers.getMMCFromString(tm.getNetworkOperator()));
                    mCell.setMNC(Helpers.getMNCFromString(tm.getNetworkOperator()));
                    mCell.setPSC(((GsmCellLocation) location).getPsc());
                }
                break;

            case TelephonyManager.PHONE_TYPE_CDMA:
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) location;
                if(cdmaCellLocation != null) {
                    mCell.setCellId(cdmaCellLocation.getBaseStationId());
                    mCell.setLAC(cdmaCellLocation.getNetworkId());
                    mCell.setSID(cdmaCellLocation.getSystemId());
                    mCell.setMNC(cdmaCellLocation.getSystemId());
                }
        }
        mCell.setTechnology(Helpers.getNetworkTypeName(tm.getNetworkType()));
        if(mCell.getFirstSeen() == 0l)
            mCell.setFirstSeen(mCell.getLastSeen());
        mCell.setLastSeen(new Date().getTime());
    }

    /**
     * Getting and comparing signal strengths between different RATs can be very
     * tricky, since they all return different ranges of values. AOS doesn't
     * specify very clearly what exactly is returned, even though people have
     * a good idea, by trial and error.
     */
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        // Update Signal Strength
        if(signalStrength.isGsm()) {
            int dbm;
            if(signalStrength.getGsmSignalStrength() <= 2 ||
                       signalStrength.getGsmSignalStrength() == NeighboringCellInfo.UNKNOWN_RSSI) {
                // Unknown signal strength, get it another way
                String[] bits = signalStrength.toString().split(" ");
                dbm = Integer.parseInt(bits[9]);
            } else {
                dbm = signalStrength.getGsmSignalStrength();
            }
            mCell.setDBM(dbm);
        } else {
//            int evdoDbm = signalStrength.getEvdoDbm();
//            int cdmaDbm = signalStrength.getCdmaDbm();
//            // Use lowest signal to be conservative
//            mCell.setDBM((cdmaDbm < evdoDbm) ? cdmaDbm : evdoDbm);
            mCell.setDBM(signalStrength.getCdmaDbm());
        }
    }

    // In DB:   No,In,Ou,IO,Do
    @Override
    public void onDataActivity(int direction) {
        switch(direction) {
            case TelephonyManager.DATA_ACTIVITY_NONE:
                mDevice.setDataActivityType(R.string.data_activity_none);
                break;
            case TelephonyManager.DATA_ACTIVITY_IN:
                mDevice.setDataActivityType(R.string.data_activity_in);
                break;
            case TelephonyManager.DATA_ACTIVITY_OUT:
                mDevice.setDataActivityType(R.string.data_activity_out);
                break;
            case TelephonyManager.DATA_ACTIVITY_INOUT:
                mDevice.setDataActivityType(R.string.data_activity_in_out);
                break;
            case TelephonyManager.DATA_ACTIVITY_DORMANT:
                mDevice.setDataActivityType(R.string.data_activity_dormant);
                break;
        }
    }

    @Override
    public void onServiceStateChanged(ServiceState serviceState) {
        super.onServiceStateChanged(serviceState);
        Log.i(TAG, "onServiceStateChanged: " + serviceState.toString());
        Log.i(TAG, "onServiceStateChanged: getOperatorAlphaLong "
                           + serviceState.getOperatorAlphaLong());
        Log.i(TAG, "onServiceStateChanged: getOperatorAlphaShort "
                           + serviceState.getOperatorAlphaShort());
        Log.i(TAG, "onServiceStateChanged: getOperatorNumeric "
                           + serviceState.getOperatorNumeric());
        Log.i(TAG, "onServiceStateChanged: getIsManualSelection "
                           + serviceState.getIsManualSelection());
        Log.i(TAG, "onServiceStateChanged: getRoaming "
                      + serviceState.getRoaming());

        switch(serviceState.getState()) {
            case ServiceState.STATE_IN_SERVICE:
                Log.i(TAG, "onServiceStateChanged: STATE_IN_SERVICE");
                break;
            case ServiceState.STATE_OUT_OF_SERVICE:
                Log.i(TAG, "onServiceStateChanged: STATE_OUT_OF_SERVICE");
                break;
            case ServiceState.STATE_EMERGENCY_ONLY:
                Log.i(TAG, "onServiceStateChanged: STATE_EMERGENCY_ONLY");
                break;
            case ServiceState.STATE_POWER_OFF:
                Log.i(TAG, "onServiceStateChanged: STATE_POWER_OFF");
                break;
        }
    }

    public boolean isTrackingCell() {
        return mTrackingCell;
    }

    public boolean isMonitoringCell() {
        return mMonitoringCell;
    }

    /**
     * @param monitor
     *         Enable/Disable monitoring
     */
    public void setCellMonitoring(boolean monitor) {
        if(monitor) {
            mMonitoringCell = true;
            Toast.makeText(mContext, mContext.getString(R.string.monitoring_cell_information),
                           Toast.LENGTH_SHORT).show();
        } else {
            mMonitoringCell = false;
            Toast.makeText(mContext,
                           mContext.getString(R.string.stopped_monitoring_cell_information),
                           Toast.LENGTH_SHORT).show();
        }
        updateNotification();
    }

    public Device getDevice() {
        return mDevice;
    }

    /**
     * Tracking Femotcell Connections
     *
     * @return boolean indicating Femtocell Connection Tracking State
     */
    public boolean isTrackingFemtocell() {
        return mTrackingFemtocell;
    }

    public void stop() {
        if(isMonitoringCell()) {
            setCellMonitoring(false);
        }
        if(isTrackingCell()) {
            setCellTracking(false);
        }
        if(isTrackingFemtocell()) {
            stopTrackingFemto();
        }
        cancelNotification();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Cell Information Tracking and database logging
     *
     * @param isEnabled
     *         Enable/Disable tracking
     */
    public void setCellTracking(boolean isEnabled) {
        if(isEnabled) {
            tm.listen(this, PhoneStateListener.LISTEN_CELL_LOCATION |
                            PhoneStateListener.LISTEN_SIGNAL_STRENGTH |
                            PhoneStateListener.LISTEN_DATA_ACTIVITY |         // No,In,Ou,IO,Do
                            PhoneStateListener.LISTEN_DATA_CONNECTION_STATE | // Di,Ct,Cd,Su
                            PhoneStateListener.LISTEN_CELL_INFO |
                            PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
            mTrackingCell = true;
            Toast.makeText(mContext, mContext.getString(R.string.tracking_cell_information),
                           Toast.LENGTH_SHORT);
        } else {
            tm.listen(this, PhoneStateListener.LISTEN_CELL_INFO |
                            PhoneStateListener.LISTEN_CELL_LOCATION |
                            PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
            mTrackingCell = false;
            Toast.makeText(mContext, mContext.getString(R.string.stopped_tracking_cell_information),
                           Toast.LENGTH_SHORT).show();
        }
        updateNotification();
    }

    /**
     * Description:    This handles the settings/choices and default preferences, when changed.
     * From the default file:
     * preferences.xml
     * And saved in the file:
     * /data/data/org.badcoders.Aimsicd/shared_prefs/com.SecUpwN.AIMSICD_preferences.xml
     *
     * For more code transparency we have added TinyDB.java as a
     * wrapper to SharedPreferences usage. Please try to use this instead.
     */
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        final String FEMTO_DETECTION = mContext.getString(R.string.pref_femto_detection_key);
        final String REFRESH = mContext.getString(R.string.pref_refresh_key);
        final String DB_VERSION = mContext.getString(R.string.pref_last_database_backup_version);
        final String OCID_KEY = mContext.getString(R.string.pref_ocid_key);

        if(key.equals(FEMTO_DETECTION)) {
            boolean trackFemtoPref = sharedPreferences.getBoolean(FEMTO_DETECTION, false);
            if(trackFemtoPref) {
                startTrackingFemto();
            } else {
                stopTrackingFemto();
            }

        } else if(key.equals(REFRESH)) {
            String refreshRate = sharedPreferences.getString(REFRESH, "15");

            int rate = Integer.parseInt(refreshRate);
            REFRESH_RATE = TimeUnit.SECONDS.toMillis(rate);
        } else if(key.equals(DB_VERSION)) {
            LAST_DB_BACKUP_VERSION = sharedPreferences.getInt(DB_VERSION, 1);
        } else if(key.equals(OCID_KEY)) {
            getOcidKey();
        }
    }

    public void getOcidKey() {
        final String OCID_KEY = mContext.getString(R.string.pref_ocid_key);
        OCID_API_KEY = prefs.getString(OCID_KEY, BuildConfig.OPEN_CELLID_API_KEY);
        if(OCID_API_KEY == null) {
            OCID_API_KEY = "NA"; // avoid null api key
        }
    }

    public List<Cell> updateNeighbouringCells() {
        List<Cell> collected;
        if(android.os.Build.VERSION.SDK_INT == 16) {
            collected = updateNeighbouringCellsForApi16();
        } else { //only > 16 below
            collected = updateNeighbouringCellsForApi17();
        }
        for(Cell cell : collected) {
            dbHelper.insertBTS(cell);
        }
        return collected;
    }

    private List<Cell> updateNeighbouringCellsForApi16() {

        List<Cell> neighboringCells = new ArrayList<>();
        List<NeighboringCellInfo> neighboringCellInfo = tm.getNeighboringCellInfo();
        neighboringCells.add(mCell);

        if(neighboringCellInfo == null)
            return neighboringCells;

        // Add NC list to DBi_measure:nc_list
        for(NeighboringCellInfo neighbourCell : neighboringCellInfo) {
            neighboringCells.add(castFromNeighboringCellInfo(neighbourCell));
        }
        return neighboringCells;
    }

    @TargetApi(17)
    private List<Cell> updateNeighbouringCellsForApi17() {

        List<Cell> neighboringCells = new ArrayList<>();
        List<CellInfo> cellsInfo = tm.getAllCellInfo();
        neighboringCells.add(mCell);

        // nothing was found, give up
        if(cellsInfo == null)
            return neighboringCells;

        if(cellsInfo.isEmpty()) {
            Log.i(TAG, "NeighbouringCellInfo was empty: start polling...");
            tm.listen(this, PhoneStateListener.LISTEN_CELL_INFO |
                            PhoneStateListener.LISTEN_CELL_LOCATION |
                            PhoneStateListener.LISTEN_DATA_CONNECTION_STATE |
                            PhoneStateListener.LISTEN_SERVICE_STATE |
                            PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        }

        // Add NC list to DBi_measure:nc_list
        for(CellInfo neighbourCell : cellsInfo) {
            if(neighbourCell instanceof CellInfoGsm) {
                neighboringCells.add(castFromCellInfoGsm(neighbourCell));
            } else if(neighbourCell instanceof CellInfoCdma) {
                neighboringCells.add(castFromCellInfoCdma(neighbourCell));
            } else if(neighbourCell instanceof CellInfoLte) {
                neighboringCells.add(castFromCellInfoLte(neighbourCell));
            }
        }
        return neighboringCells;
    }

    public void compareLac(CellLocation location) {

        switch(mDevice.getPhoneID()) {
            case TelephonyManager.PHONE_TYPE_GSM:
                GsmCellLocation gsmCellLocation = (GsmCellLocation) location;
                if(gsmCellLocation != null) {
                    mCell.setLAC(gsmCellLocation.getLac());
                    mCell.setCellId(gsmCellLocation.getCid());
                    // Check if LAC is ok
                    boolean lacOK = dbHelper.checkLAC(getCell());
                    if(!lacOK) {
                        mChangedLAC = true;
                        dbHelper.toEventLog(1, "Changing LAC");
                        updateNotification();
                    } else {
                        mChangedLAC = false;
                    }

                    mCellIdNotInOpenDb = !dbHelper.openCellExists(getCell().getCellId());

                    if(mCellIdNotInOpenDb) {
                        dbHelper.toEventLog(2, "CellId not found in imported cell");

                        Log.i(TAG, "ALERT: Connected to unknown CID not in DBe_import: " + getCell().getCellId());
                        updateNotification();
                    }
                }
                break;

            case TelephonyManager.PHONE_TYPE_CDMA:
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) location;
                if(cdmaCellLocation != null) {
                    mCell.setLAC(cdmaCellLocation.getNetworkId());
                    mCell.setCellId(cdmaCellLocation.getBaseStationId());

                    boolean lacOK = dbHelper.checkLAC(getCell());
                    if(!lacOK) {
                        mChangedLAC = true;
                        dbHelper.toEventLog(1, "Changing LAC");
                        updateNotification();
                    } else {
                        mChangedLAC = false;
                    }
                }
                break;
        }
    }

    public void refreshDevice() {
        mDevice.refreshDeviceInfo(tm);
    }

    private void loadPreferences() {
        // defaults are given by:  getBoolean(key, default if not exist)
        boolean trackFemtoPref = prefs.getBoolean(
                mContext.getString(R.string.pref_femto_detection_key), false);
        boolean trackCellPref = prefs.getBoolean(mContext.getString(R.string.pref_enable_cell_key),
                                                 true);
        boolean monitorCellPref = prefs.getBoolean(
                mContext.getString(R.string.pref_enable_cell_monitoring_key), true);

        LAST_DB_BACKUP_VERSION = prefs.getInt(
                mContext.getString(R.string.pref_last_database_backup_version), 1);
        String refreshRate = prefs.getString(mContext.getString(R.string.pref_refresh_key), "25");

        long t = Long.parseLong(refreshRate);

        REFRESH_RATE = TimeUnit.SECONDS.toMillis(t);
        getOcidKey();

        if(trackFemtoPref) {
            startTrackingFemto();
        }
        if(trackCellPref) {
            setCellTracking(true);
        }
        if(monitorCellPref) {
            setCellMonitoring(true);
        }
    }

    public void insertCurrentCellToDb() {
        // This only logs a BTS if we have GPS lock
         if(mMonitoringCell && NetworkUtil.isGpsLocation(mContext)) {
             // This also checks that the lac are cid are not in DB before inserting
             dbHelper.insertBTS(mCell);
         }
    }

    public void onLocationChanged(Location loc) {
        if(Build.VERSION.SDK_INT >= 18) {
            mCell = DeviceApi18.loadCellInfo(tm, mCell);
        } else if(Build.VERSION.SDK_INT == 17) {
            mCell = DeviceApi17.loadCellInfo(tm, mCell);
        } else {
            CellLocation cellLocation = tm.getCellLocation();

            switch(mDevice.getPhoneID()) {
                case TelephonyManager.PHONE_TYPE_GSM:
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    mCell.setCellId(gsmCellLocation.getCid());
                    mCell.setLAC(gsmCellLocation.getLac());
                    mCell.setPSC(gsmCellLocation.getPsc());
                    mCell.setMCC(Helpers.getMMCFromString(tm.getNetworkOperator()));
                    mCell.setMNC(Helpers.getMNCFromString(tm.getNetworkOperator()));
                    mCell.setTechnology("GSM");
                    break;

                case TelephonyManager.PHONE_TYPE_CDMA:
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                    mCell.setCellId(cdmaCellLocation.getBaseStationId());
                    mCell.setLAC(cdmaCellLocation.getNetworkId());
                    mCell.setSID(cdmaCellLocation.getSystemId());
                    mCell.setMCC(Helpers.getMMCFromString(tm.getNetworkOperator()));
                    mCell.setMNC(cdmaCellLocation.getSystemId());
                    mCell.setTechnology("CDMA");
                    break;
            }

            mCell.setLon(loc.getLongitude());
            mCell.setLat(loc.getLatitude());
            mCell.setSpeed(loc.getSpeed());
            mCell.setAccuracy(loc.getAccuracy());
        }
    }

    /**
     * Cancel and remove the persistent notification
     */
    public void cancelNotification() {
        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null) {
            notificationManager.cancel(MyNotification.NOTIFICATION_ID);
        }
    }

    public void updateNotification() {
        String tickerText;
        String contentText = mContext.getString(R.string.phone_type) + mDevice.getPhoneType();

        if(mFemtoDetected) {
            Status.setCurrentStatus(Status.Type.ALARM, this.mContext);
        } else if(mChangedLAC) {
            Status.setCurrentStatus(Status.Type.MEDIUM, this.mContext);
            contentText = mContext.getString(R.string.hostile_service_area_changing_lac_detected);
        } else if(mCellIdNotInOpenDb) {
            Status.setCurrentStatus(Status.Type.MEDIUM, this.mContext);
            contentText = mContext.getString(R.string.cell_id_doesnt_exist_in_db);
        } else if(mTrackingFemtocell || mTrackingCell || mMonitoringCell) {
            Status.setCurrentStatus(Status.Type.NORMAL, this.mContext);
            if(mTrackingFemtocell) {
                contentText = mContext.getString(R.string.femtocell_detection_active);
            } else if(mTrackingCell) {
                contentText = mContext.getString(R.string.cell_tracking_active);
            } else {
                contentText = mContext.getString(R.string.cell_monitoring_active);
            }
        } else {
            Status.setCurrentStatus(Status.Type.IDLE, this.mContext);
        }

        switch(Status.getStatus()) {

            case IDLE:
                contentText = mContext.getString(R.string.phone_type) + mDevice.getPhoneType();
                tickerText = mContext.getResources().getString(
                        R.string.app_name_short) + " " + mContext.getString(R.string.status_idle);
                break;

            case NORMAL:
                tickerText = mContext.getResources().getString(
                        R.string.app_name_short) + " " + mContext.getString(R.string.status_good);
                break;

            case MEDIUM:
                // Initialize tickerText as the app name string
                // See multiple detection comments above.
                tickerText = mContext.getResources().getString(R.string.app_name_short);
                if(mChangedLAC) {
                    //Append changing LAC text
                    contentText = mContext.getString(
                            R.string.hostile_service_area_changing_lac_detected);
                    tickerText += " - " + contentText;
                    // See #264 and ask He3556
                    //} else if (mNoNCList)  {
                    //    tickerText += " - BTS doesn't provide any neighbors!";
                    //    contentText = "CID: " + cellid + " is not providing a neighboring cell list!";

                } else if(mCellIdNotInOpenDb) {
                    //Append Cell ID not existing in external db text
                    contentText = mContext.getString(R.string.cell_id_doesnt_exist_in_db);
                    tickerText += " - " + contentText;
                }
                break;

            case ALARM:
                tickerText = mContext.getResources().getString(
                        R.string.app_name_short) + " - " + mContext.getString(
                        R.string.alert_threat_detected); // Hmm, this is vague!
                if(mFemtoDetected) {
                    contentText = mContext.getString(R.string.alert_femtocell_connection_detected);
                } else {
                    contentText = mContext.getString(R.string.alert_silent_sms_detected);
                }
                break;

            default:
                tickerText = mContext.getResources().getString(R.string.main_app_name);
                break;
        }

        new MyNotification(mContext, tickerText, contentText);
    }

    /**
     * Start FemtoCell detection tracking (For CDMA Devices ONLY!)
     */
    public void startTrackingFemto() {

        /* Check if it is a CDMA phone */
        if(mDevice.getPhoneID() != TelephonyManager.PHONE_TYPE_CDMA) {
            Toast.makeText(mContext,
                           mContext.getString(R.string.femtocell_only_on_cdma_devices),
                           Toast.LENGTH_SHORT).show();
            return;
        }

        mTrackingFemtocell = true;

        tm.listen(this,
                  PhoneStateListener.LISTEN_CELL_LOCATION | PhoneStateListener.LISTEN_SERVICE_STATE);
        updateNotification();
    }

    /**
     * Stop FemtoCell detection tracking (For CDMA Devices ONLY!)
     */
    public void stopTrackingFemto() {
        tm.listen(this, PhoneStateListener.LISTEN_NONE);
        mTrackingFemtocell = false;
        updateNotification();
    }

    private boolean isConnectedToCdmaFemto(ServiceState s) {
        if(s == null) {
            return false;
        }

        /* Get International Roaming indicator
         * if indicator is not 0 return false
         */

        /* Get the radio technology */
        int networkType = mCell.getNetType();

        /* Check if it is EvDo network */
        boolean evDoNetwork = isEvDoNetwork(networkType);

        /* If it is not an evDo network check the network ID range.
         * If it is connected to Femtocell, the NID should be between [0xfa, 0xff)
         */
        if(!evDoNetwork) {
            /* get network ID */
            if(tm != null) {
                CdmaCellLocation c = (CdmaCellLocation) tm.getCellLocation();

                if(c != null) {
                    int networkID = c.getNetworkId();
                    int FEMTO_NID_MAX = 0xff;
                    int FEMTO_NID_MIN = 0xfa;
                    return !((networkID < FEMTO_NID_MIN) || (networkID >= FEMTO_NID_MAX));

                } else {
                    Log.v(TAG, "Cell location info is null.");
                    return false;
                }
            } else {
                Log.v(TAG, "Telephony Manager is null.");
                return false;
            }
        }

        /* if it is an evDo network */
        //
        else {
            /* get network ID */
            if(tm != null) {
                CdmaCellLocation c = (CdmaCellLocation) tm.getCellLocation();

                if(c != null) {
                    int networkID = c.getNetworkId();

                    int FEMTO_NID_MAX = 0xff;
                    int FEMTO_NID_MIN = 0xfa;
                    return !((networkID < FEMTO_NID_MIN) || (networkID >= FEMTO_NID_MAX));
                } else {
                    Log.v(TAG, "Cell location info is null.");
                    return false;
                }
            } else {
                Log.v(TAG, "Telephony Manager is null.");
                return false;
            }
        }

    }

    /**
     * Confirmation of connection to an EVDO Network
     *
     * @return EVDO network connection returns TRUE
     */
    private boolean isEvDoNetwork(int networkType) {
        return (networkType == TelephonyManager.NETWORK_TYPE_EVDO_0) ||
                       (networkType == TelephonyManager.NETWORK_TYPE_EVDO_A) ||
                       (networkType == TelephonyManager.NETWORK_TYPE_EVDO_B) ||
                       (networkType == TelephonyManager.NETWORK_TYPE_EHRPD);
    }


    private Cell castFromNeighboringCellInfo(NeighboringCellInfo cellInfo) {
        Cell cell = new Cell();
        cell.setCellId(cellInfo.getCid());
        cell.setLAC(cellInfo.getLac());
        cell.setDBM(cellInfo.getRssi());
        cell.setNetType(cellInfo.getNetworkType());
        cell.setLastSeen(new Date().getTime());
        cell.setPSC(cellInfo.getPsc());
        cell.setTechnology(Helpers.getNetworkTypeName(cellInfo.getNetworkType()));
        return cell;
    }

    @TargetApi(17)
    private Cell castFromCellInfoLte(CellInfo cellInfo) {
        CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
        Cell cell = new Cell();
        cell.setCellId(cellInfoLte.getCellIdentity().getCi());
        cell.setPSC(cellInfoLte.getCellIdentity().getPci());
        cell.setLAC(cellInfoLte.getCellIdentity().getTac());
        cell.setMCC(cellInfoLte.getCellIdentity().getMcc());
        cell.setMNC(cellInfoLte.getCellIdentity().getMnc());
        cell.setDBM(cellInfoLte.getCellSignalStrength().getDbm());
        cell.setTimingAdvance(cellInfoLte.getCellSignalStrength().getTimingAdvance());
        cell.setLastSeen(new Date().getTime());
        cell.setTechnology("LTE");
        return cell;
    }

    @TargetApi(17)
    private Cell castFromCellInfoCdma(CellInfo cellInfo) {
        CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
        Cell cell = new Cell();
        cell.setCellId(cellInfoCdma.getCellIdentity().getBasestationId());
        cell.setLAC(cellInfoCdma.getCellIdentity().getNetworkId());
        cell.setMCC(cellInfoCdma.getCellIdentity().getSystemId());
        cell.setMNC(cellInfoCdma.getCellIdentity().getNetworkId());
        cell.setDBM(cellInfoCdma.getCellSignalStrength().getDbm());
        cell.setLastSeen(new Date().getTime());
        cell.setTechnology("CDMA");
        return cell;
    }

    @TargetApi(17)
    private Cell castFromCellInfoGsm(CellInfo cellInfo) {
        CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
        Cell cell = new Cell();
        cell.setCellId(cellInfoGsm.getCellIdentity().getCid());
        cell.setLAC(cellInfoGsm.getCellIdentity().getLac());
        cell.setMCC(cellInfoGsm.getCellIdentity().getMcc());
        cell.setMNC(cellInfoGsm.getCellIdentity().getMnc());
        cell.setDBM(cellInfoGsm.getCellSignalStrength().getDbm());
        cell.setLastSeen(new Date().getTime());
        cell.setTechnology("GSM");
        return cell;
    }

    public static Cell getCell() {
        return mCell;
    }

    public static void setCell(Cell mCell) {
        CellTracker.mCell = mCell;
    }

}
