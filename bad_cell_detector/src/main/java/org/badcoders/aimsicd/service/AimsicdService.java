/*
 * Portions of this software have been copied and modified from
 * https://github.com/illarionov/SamsungRilMulticlient
 * Copyright (C) 2014 Alexey Illarionov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.badcoders.aimsicd.service;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.WindowManager;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.rilexecutor.RilExecutor;
import org.badcoders.aimsicd.smsdetection.SmsDetector;
import org.badcoders.aimsicd.utils.GeoLocation;

/**
 * This starts the Aimsicd service to check for SMS and track
 * cells with or without GPS enabled.
 */
public class AimsicdService extends Service {

    private static final String TAG = "AimsicdService";

    public static final String SHARED_PREFERENCES_BASENAME = "org.badcoders.aimsicd_preferences";
    public static final String UPDATE_DISPLAY = "UPDATE_DISPLAY";

    private final AimscidBinder mBinder = new AimscidBinder();

    private CellTracker mCellTracker;
    private LocationTracker mLocationTracker;
    private RilExecutor mRilExecutor;
    private SmsDetector smsdetector;

    private boolean isLocationRequestShowing = false;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class AimscidBinder extends Binder {

        public AimsicdService getService() {
            return AimsicdService.this;
        }
    }

    @Override
    public void onCreate() {
        mLocationTracker = new LocationTracker(this, mLocationListener);
        mRilExecutor = new RilExecutor(this);
        mCellTracker = new CellTracker(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mCellTracker.stop();
        mLocationTracker.stop();
        mRilExecutor.stop();

        if(SmsDetector.getSmsDetectionState()) {
            smsdetector.stopSmsDetection();
        }
        stopSelf();
    }

    public GeoLocation lastKnownLocation() {
        return mLocationTracker.lastKnownLocation();
    }

    public RilExecutor getRilExecutor() {
        return mRilExecutor;
    }

    public CellTracker getCellTracker() {
        return mCellTracker;
    }

    public Cell getCell() {
        return mCellTracker.getCell();
    }

    public void setCell(Cell cell) {
        mCellTracker.setCell(cell);
    }

    public boolean isTrackingCell() {
        return mCellTracker.isTrackingCell();
    }

    public boolean isMonitoringCell() {
        return mCellTracker.isMonitoringCell();
    }

    public void setCellMonitoring(boolean monitor) {
        mCellTracker.setCellMonitoring(monitor);
    }

    public boolean isTrackingFemtocell() {
        return mCellTracker.isTrackingFemtocell();
    }

    public void setTrackingFemtocell(boolean track) {
        if(track) {
            mCellTracker.startTrackingFemto();
        } else {
            mCellTracker.stopTrackingFemto();
        }
    }

    // SMS Detection Thread
    public boolean isSilentSmsDetectionEnabled() {
        return SmsDetector.getSmsDetectionState();
    }

    public void startSmsTracking() {
        if(!isSilentSmsDetectionEnabled()) {
            Log.i(TAG, "Sms Detection Thread Started");
            smsdetector = new SmsDetector(this);
            smsdetector.startSmsDetection();
        }
    }

    public void stopSmsTracking() {
        if(isSilentSmsDetectionEnabled()) {
            smsdetector.stopSmsDetection();
            Log.i(TAG, "Sms Detection Thread Stopped");
        }
    }

    /**
     * Cell Information Tracking and database logging
     *
     * @param track
     *         Enable/Disable tracking
     */
    public void setCellTracking(boolean track) {
        mCellTracker.setCellTracking(track);

        if(track) {
            mLocationTracker.start();
        } else {
            mLocationTracker.stop();
        }
    }

    public void checkLocationServices() {
        if(!mLocationTracker.isGPSOn() && mCellTracker.isTrackingCell()) {
            isLocationRequestShowing = true; // prevent showing if gps is on
            enableLocationServices();
        }
    }

    private void enableLocationServices() {
        if(isLocationRequestShowing)
            return; // only show dialog once

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.location_error_message)
                .setTitle(R.string.location_error_title)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isLocationRequestShowing = false;
                        Intent gpsSettings = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        gpsSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(gpsSettings);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        isLocationRequestShowing = false;
                        setCellTracking(false);
                    }
                });
        AlertDialog alert = builder.create();
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();
        isLocationRequestShowing = true;
    }

    LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location loc) {
            mCellTracker.getCell().setSpeed(loc.getSpeed());
            mCellTracker.getCell().setAccuracy(loc.getAccuracy());
            mCellTracker.insertCurrentCellToDb();
        }

        @Override
        public void onProviderDisabled(String provider) {
            if(mCellTracker.isTrackingCell() && provider.equals(LocationManager.GPS_PROVIDER)) {
                mCellTracker.setCellTracking(false);
            }
        }

        @Override
        public void onProviderEnabled(String s) {
            if(!mCellTracker.isTrackingCell() && LocationManager.GPS_PROVIDER.equals(s)) {
                mCellTracker.setCellTracking(true);
                mCellTracker.setCellMonitoring(true);
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }
    };
}
