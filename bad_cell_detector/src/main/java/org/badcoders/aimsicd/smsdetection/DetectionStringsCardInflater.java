package org.badcoders.aimsicd.smsdetection;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.BaseInflaterAdapter;
import org.badcoders.aimsicd.adapters.IAdapterViewInflater;

public class DetectionStringsCardInflater implements IAdapterViewInflater<DetectionStringsData> {

    @Override
    public View inflate(final BaseInflaterAdapter<DetectionStringsData> adapter,
                        final int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.detection_strings_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final DetectionStringsData item = adapter.getTItem(pos);
        holder.updateDisplay(item);

        return convertView;
    }

    private class ViewHolder {

        private final View mRootView;
        private final TextView detection;
        private final TextView type;


        public ViewHolder(View rootView) {
            mRootView = rootView;

            detection = (TextView) mRootView.findViewById(R.id.tv_det_str_info);
            type = (TextView) mRootView.findViewById(R.id.tv_det_type_info);

            rootView.setTag(this);
        }

        public void updateDisplay(DetectionStringsData item) {
            detection.setText(item.getDetectionString());
            type.setText(item.getDetectionType());
        }
    }
}
