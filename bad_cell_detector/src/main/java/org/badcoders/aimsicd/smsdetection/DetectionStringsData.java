package org.badcoders.aimsicd.smsdetection;

public class DetectionStringsData {

    private String mDetectionString;
    private String mDetectionType;

    public DetectionStringsData(String pString, String pType) {
        mDetectionString = pString;
        mDetectionType = pType;
    }

    public String getDetectionString() {
        return mDetectionString;
    }

    public String getDetectionType() {
        return mDetectionType;
    }
}
