package org.badcoders.aimsicd.smsdetection;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.acra.ACRA;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.constants.DBTableColumnIds;

import java.util.ArrayList;
import java.util.List;

public class AdvancedUserActivity extends Activity {

    private static final String TAG = "AdvancedUserActivity";

    private ListView listViewAdv;
    private DatabaseAdapter dbAccess;
    private EditText editAdvUserDet;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_user);
        dbAccess = new DatabaseAdapter(getApplicationContext());

        Button insertButton = (Button) findViewById(R.id.btn_insert);
        editAdvUserDet = (EditText) findViewById(R.id.edit_adv_user_string);
        spinner = (Spinner) findViewById(R.id.spinner);
        listViewAdv = (ListView) findViewById(R.id.listView_Adv_Activity);
        List<AdvanceUserItems> msgItems;

        try {
            msgItems = dbAccess.getDetectionStrings();
        } catch (Exception ee) {
            Log.e(TAG, "Database error: " +ee.getMessage(), ee);
            msgItems = new ArrayList<>();
            AdvanceUserItems advUserItems = new AdvanceUserItems();
            advUserItems.setDetectionString("NO DATA");
            advUserItems.setDetectionType("No TYPE");
            msgItems.add(advUserItems);
        }

        listViewAdv.setAdapter(new AdvanceUserBaseAdapter(getApplicationContext(), msgItems));

        listViewAdv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> a, View v, int position, long id) {
                Object o = listViewAdv.getItemAtPosition(position);
                AdvanceUserItems itemDetails = (AdvanceUserItems) o;

                String itemDetail = itemDetails.getDetectionString();

                if (dbAccess.deleteDetectionString(itemDetails.getDetectionString())) {
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.deleted) + ": " + itemDetail, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_to_delete)
                            + " " + itemDetail, Toast.LENGTH_SHORT).show();
                }

                try {
                    loadDbString();
                } catch (Exception e) {
                    ACRA.getErrorReporter().handleSilentException(e);
                    Log.d(TAG, "Error loading db string" + e.getMessage(), e);
                }
                return false;
            }
        });


        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editAdvUserDet.getText().toString().contains("\"")) {
                    Toast.makeText(getApplicationContext(), R.string.double_quote_will_cause_db_error,
                            Toast.LENGTH_SHORT).show();
                } else {
                    ContentValues store_new_sms_string = new ContentValues();
                    store_new_sms_string.put(DBTableColumnIds.DETECTION_STRINGS_LOGCAT_STRING,
                                             editAdvUserDet.getText().toString());

                    store_new_sms_string.put(DBTableColumnIds.DETECTION_STRINGS_SMS_TYPE,
                                             spinner.getSelectedItem().toString());

                    if (dbAccess.insertNewDetectionString(store_new_sms_string)) {
                        Toast.makeText(getApplicationContext(), R.string.the_string_was_added_to_db,
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.failed_to_add_the_string_to_db,
                                Toast.LENGTH_SHORT).show();
                    }

                    try {
                        loadDbString();
                    } catch (Exception ee) {
                        Log.e(TAG, ee.getMessage(), ee);
                    }
                }
            }
        });
    }

    /**
     * Reload ListView with new database values
     */
    public void loadDbString() {
        try {
            /* There should be at least 1 detection string in db so not to cause an error */
            List<AdvanceUserItems> newmsglist = dbAccess.getDetectionStrings();
            listViewAdv.setAdapter(new AdvanceUserBaseAdapter(getApplicationContext(), newmsglist));
        } catch (Exception ee) {
            Log.e(TAG, ee.getMessage(), ee);
        }
    }
}
