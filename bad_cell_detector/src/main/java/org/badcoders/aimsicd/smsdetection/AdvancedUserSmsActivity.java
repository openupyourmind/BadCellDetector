package org.badcoders.aimsicd.smsdetection;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.constants.DBTableColumnIds;

import java.util.ArrayList;
import java.util.List;

public class AdvancedUserSmsActivity extends Activity {

    private static final String TAG = "AdvancedUserSmsActivity";
    private ListView listViewAdv;
    private DatabaseAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_sms_user);

        dbAdapter = new DatabaseAdapter(getApplicationContext());

        listViewAdv = (ListView) findViewById(R.id.listView_Adv_Sms_Activity);
        List<CapturedSmsData> msgitems = new ArrayList<>();

        try {
            Cursor smscur = dbAdapter.returnSmsData();
            if(smscur.getCount() > 0) {
                while(smscur.moveToNext()) {
                    CapturedSmsData getdata = new CapturedSmsData();
                    getdata.setId(smscur.getLong(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_ID)));
                    getdata.setSmsTimestamp(smscur.getLong(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_TIMESTAMP)));
                    getdata.setSmsType(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_SMS_TYPE)));
                    getdata.setSenderNumber(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_SENDER_NUMBER)));
                    getdata.setSenderMsg(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_SENDER_MSG)));
                    getdata.setLAC(smscur.getInt(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_LAC)));
                    getdata.setCellId(smscur.getInt(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_CID)));
                    getdata.setTechnology(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_RAT)));
                    getdata.setRoamingStatus(smscur.getInt(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_ROAM_STATE)) > 0);
                    getdata.setLat(smscur.getDouble(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_GPS_LAT)));
                    getdata.setLon(smscur.getDouble(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_GPS_LON)));
                    msgitems.add(getdata);
                }
            }
            smscur.close();

        } catch(Exception ee) {
            Log.e(TAG, "DB ERROR>>>>" + ee.toString());
        }


        listViewAdv.setAdapter(new AdvanceUserBaseSmsAdapter(getApplicationContext(), msgitems));

        listViewAdv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> a, View v, int position, long id) {
                Object o = listViewAdv.getItemAtPosition(position);
                CapturedSmsData obj_itemDetails = (CapturedSmsData) o;

                if(dbAdapter.deleteDetectedSms(obj_itemDetails.getId())) {
                    Toast.makeText(getApplicationContext(),
                                   "Deleted Sms Id = \n" + obj_itemDetails.getId(),
                                   Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to Delete",
                                   Toast.LENGTH_SHORT).show();
                }

                try {
                    loadDbString();
                } catch(Exception e) {
                    Log.e(TAG, "problem loadind db ", e);
                }
                return false;
            }

        });

    }

    public void loadDbString() {
        List<CapturedSmsData> newmsglist = new ArrayList<>();

        try {
            Cursor smscur = dbAdapter.returnSmsData();

            if(smscur.getCount() > 0) {
                while(smscur.moveToNext()) {
                    CapturedSmsData getdata = new CapturedSmsData();
                    getdata.setId(smscur.getLong(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_ID)));
                    getdata.setSmsTimestamp(smscur.getLong(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_TIMESTAMP)));
                    getdata.setSmsType(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_SMS_TYPE)));
                    getdata.setSenderNumber(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_SENDER_NUMBER)));
                    getdata.setSenderMsg(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_SENDER_MSG)));
                    getdata.setLAC(smscur.getInt(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_LAC)));
                    getdata.setCellId(smscur.getInt(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_CID)));
                    getdata.setTechnology(smscur.getString(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_RAT)));
                    getdata.setRoamingStatus(smscur.getInt(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_ROAM_STATE))> 0);
                    getdata.setLat(smscur.getDouble(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_GPS_LAT)));
                    getdata.setLon(smscur.getDouble(smscur.getColumnIndex(DBTableColumnIds.SMS_DATA_GPS_LON)));

                    newmsglist.add(getdata);
                }
            }

            smscur.close();
            listViewAdv.setAdapter(
                    new AdvanceUserBaseSmsAdapter(getApplicationContext(), newmsglist));
        } catch(Exception e) {
            Log.e(TAG, "DB error", e);
        }
    }
}
