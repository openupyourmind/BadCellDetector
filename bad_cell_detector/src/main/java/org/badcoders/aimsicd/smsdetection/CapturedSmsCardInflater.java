package org.badcoders.aimsicd.smsdetection;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.BaseInflaterAdapter;
import org.badcoders.aimsicd.adapters.IAdapterViewInflater;
import org.badcoders.aimsicd.utils.Formatter;

public class CapturedSmsCardInflater implements IAdapterViewInflater<CapturedSmsData> {

    @Override
    public View inflate(final BaseInflaterAdapter<CapturedSmsData> adapter,
                        final int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.detection_sms_db_listview, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final CapturedSmsData item = adapter.getTItem(pos);
        holder.updateDisplay(item);

        return convertView;
    }

    private class ViewHolder {

        private final View mRootView;
        private final TextView smsd_timestamp, smsd_smstype, smsd_number, smsd_data,
                smsd_lac, smsd_cid, smsd_rat, smsd_roam, smsd_lat, smsd_lon;

        public ViewHolder(View rootView) {
            mRootView = rootView;

            smsd_timestamp = (TextView) mRootView.findViewById(R.id.tv_smsdata_timestamp);
            smsd_smstype = (TextView) mRootView.findViewById(R.id.tv_smsdata_smstype);
            smsd_number = (TextView) mRootView.findViewById(R.id.tv_smsdata_number);
            smsd_data = (TextView) mRootView.findViewById(R.id.tv_smsdata_msg);
            smsd_lac = (TextView) mRootView.findViewById(R.id.tv_smsdata_lac);
            smsd_cid = (TextView) mRootView.findViewById(R.id.tv_smsdata_cid);
            smsd_rat = (TextView) mRootView.findViewById(R.id.tv_smsdata_nettype);
            smsd_roam = (TextView) mRootView.findViewById(R.id.tv_smsdata_roaming);
            smsd_lat = (TextView) mRootView.findViewById(R.id.tv_smsdata_lat);
            smsd_lon = (TextView) mRootView.findViewById(R.id.tv_smsdata_lon);

            rootView.setTag(this);
        }

        public void updateDisplay(CapturedSmsData item) {
            smsd_timestamp.setText(Formatter.formatDateTime(item.getSmsTimestamp()));
            smsd_smstype.setText(item.getSmsType());
            smsd_number.setText(item.getSenderNumber());
            smsd_data.setText(item.getSenderMsg());
            smsd_lac.setText(String.valueOf(item.getLAC()));
            smsd_cid.setText(String.valueOf(item.getCellId()));
            smsd_rat.setText(item.getTechnology());

            smsd_roam.setText(item.getRoamingStatus() == true ? R.string.yes : R.string.no);
            smsd_lat.setText(Formatter.formatLocation(item.getLat()));
            smsd_lon.setText(Formatter.formatLocation(item.getLon()));
        }
    }

}
