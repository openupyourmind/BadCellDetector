package org.badcoders.aimsicd.smsdetection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.badcoders.aimsicd.R;

import java.util.List;

public class AdvanceUserBaseAdapter extends BaseAdapter {
    private static List<AdvanceUserItems> detectionItemDetails;

    private LayoutInflater l_Inflater;

    public AdvanceUserBaseAdapter(Context context, List<AdvanceUserItems> results) {
        detectionItemDetails = results;
        l_Inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return detectionItemDetails.size();
    }

    public Object getItem(int position) {
        return detectionItemDetails.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = l_Inflater.inflate(R.layout.adv_user_strings_list, parent, false);
            holder = new ViewHolder();
            holder.detectionString = (TextView) convertView.findViewById(R.id.tv_adv_list_det_str);
            holder.detectionType = (TextView) convertView.findViewById(R.id.tv_adv_list_det_type);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.detectionString.setText(detectionItemDetails.get(position).getDetectionString());
        holder.detectionType.setText(detectionItemDetails.get(position).getDetectionType());
        return convertView;
    }

    static class ViewHolder {
        TextView detectionString,
                detectionType;
    }
}
