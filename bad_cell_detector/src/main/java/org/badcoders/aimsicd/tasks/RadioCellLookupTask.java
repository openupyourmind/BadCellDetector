package org.badcoders.aimsicd.tasks;

import android.os.AsyncTask;

import com.github.kevinsawicki.http.HttpRequest;

import org.badcoders.aimsicd.model.Cell;

public class RadioCellLookupTask extends AsyncTask<Cell, Integer, Integer> {

    protected String body;

    @Override
    protected Integer doInBackground(Cell... params) {
        String url = buildRequest(params[0]);
        if(url == null) {
            return -1;
        }

        int code;
        try {
            HttpRequest request = HttpRequest.get(url);
            code = request.code();
            this.body = request.body();
        } catch(RuntimeException re) {
            return -1000;
        }
        return code;
    }

    private String buildRequest(Cell cell) {
        String url = "https://radiocells.org/api/cell_info?";
        if(cell.getCellId() != -1) {
            url += "cid=" + cell.getCellId();
        } else {
            return null;
        }
        if(cell.getMCC() != -1) {
            url += "&mcc=" + cell.getMCC();
        }
        if(cell.getMNC() != -1) {
            url += "&mnc=" + cell.getMNC();
        }
        return url;
    }

}
