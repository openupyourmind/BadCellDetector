package org.badcoders.aimsicd.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;

import org.badcoders.aimsicd.Aimsicd;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.service.CellTracker;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;
import org.badcoders.aimsicd.utils.GeoLocation;

import java.io.File;
import java.io.IOException;

public class OpenCellIdDownloadTask extends AsyncTask<Cell, Integer, Integer> {

    private static final String TAG = "OpenCellIdDownloadTask";

    private Context mContext;
    private DatabaseAdapter dbAdapter;
    private File output;

    public OpenCellIdDownloadTask(Context context, DatabaseAdapter adapter) {
        this.mContext = context;
        this.dbAdapter = adapter;
    }

    @Override
    protected Integer doInBackground(Cell... cell) {

        String urlLink = buildRequest(cell[0]);
        if(urlLink == null) {
            return -1010101; //failed to build request
        }

        try {
            output =  File.createTempFile("ocidCellsFile", ".tmp");
            output.deleteOnExit();
            return HttpRequest.get(urlLink).receive(output).code();
        } catch(RuntimeException | IOException re) {
            Log.w(TAG, re.toString());
            return -1000;
        }
    }

    @Override
    protected void onPostExecute(Integer result) {
        if(result == 200) {
            if(dbAdapter.populateDBeImport(output.getAbsolutePath())) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                        mContext);

                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("cell_data_downloaded", true);
                editor.apply();
                dbAdapter.close();
                Toast.makeText(mContext, mContext.getString(
                        R.string.cell_data_successfully_received_markers_updated),
                               Toast.LENGTH_SHORT).show();
            }
            // just for visualisation
            Aimsicd.mProgressBar.setMax(100);
            Aimsicd.mProgressBar.setProgress(100);

            Aimsicd.mProgressBar.setProgress(0);
        } else if(result == -1010101) {
            Toast.makeText(mContext, R.string.you_need_opencellid_api_key,
                           Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(mContext, mContext.getString(
                    R.string.unknown_problem_connecting_to_online_resource_encountered),
                           Toast.LENGTH_LONG).show();
        }

        output.delete();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Aimsicd.mProgressBar.setProgress(values[0]);
        Aimsicd.mProgressBar.setMax(values[1]);
    }

    private String buildRequest(Cell cell) {

        if(!"NA".equals(CellTracker.OCID_API_KEY)) {
            double earthRadius = 6371.01; // [Km]
            int radius = 6; // Use a 6 Km radius with center at GPS location.

            if(Double.doubleToRawLongBits(cell.getLat()) != 0 &&
                       Double.doubleToRawLongBits(cell.getLon()) != 0) {
                //New GeoLocation object to find bounding Coordinates
                GeoLocation currentLoc = GeoLocation.fromDegrees(cell.getLat(), cell.getLon());

                //Calculate the Bounding Box Coordinates using an N Km "radius" //0=min, 1=max
                GeoLocation[] boundingCoords = currentLoc.boundingCoordinates(radius, earthRadius);
                String boundParameter;

                //Request OpenCellID data for Bounding Coordinates (0 = min, 1 = max)
                boundParameter =
                                Formatter.formatLocation(boundingCoords[0].getLatitudeInDegrees()) + "," +
                                Formatter.formatLocation(boundingCoords[0].getLongitudeInDegrees()) + "," +
                                Formatter.formatLocation(boundingCoords[1].getLatitudeInDegrees()) + "," +
                                Formatter.formatLocation(boundingCoords[1].getLongitudeInDegrees());

                String url = "http://www.opencellid.org/cell/getInArea?"
                                     + "key=" + CellTracker.OCID_API_KEY
                                     + "&BBOX=" + boundParameter;

                if(cell.getMCC() != -1) {
                    url += "&mcc=" + cell.getMCC();
                }
                if(cell.getMNC() != -1) {
                    url += "&mnc=" + cell.getMNC();
                }
                url += "&format=csv";
                return url;
            }
        }
        return null;
    }
}
