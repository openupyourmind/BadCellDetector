/*
Copyright (c) 2015-* by agilob

ISC License - The acceptable one

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD
TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/
package org.badcoders.aimsicd.tasks;

import com.google.gson.JsonObject;

import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;

import java.text.ParseException;
import java.util.Date;

public class RadioCellHelper {

    public static Cell getCellFromJson(JsonObject json) {
        Cell cell = new Cell();
        JsonObject jsonObject = json.getAsJsonObject("geometry").getAsJsonObject();
        cell.setLat(jsonObject.getAsJsonArray("coordinates").get(1).getAsDouble());
        cell.setLon(jsonObject.getAsJsonArray("coordinates").get(0).getAsDouble());

        jsonObject = json.getAsJsonObject("properties");
        cell.setMCC(jsonObject.get("mcc").getAsInt());
        cell.setMNC(jsonObject.get("mnc").getAsInt());
        cell.setCellId(jsonObject.get("cid").getAsInt());
        cell.setPSC(jsonObject.get("psc").getAsInt());
        cell.setLAC(jsonObject.get("area").getAsInt());
        cell.setTechnology(getTechnology(jsonObject.get("technology").getAsString()));
        cell.setSamples(jsonObject.get("measurements").getAsInt());

        try {
            cell.setFirstSeen(Formatter.parseDate(jsonObject.get("created_at").getAsString()).getTime());
        } catch(ParseException e) {
            cell.setFirstSeen(0l);
        }

        try {
            cell.setLastSeen(Formatter.parseDate(jsonObject.get("last_updated").getAsString()).getTime());
        } catch(ParseException e) {
            cell.setLastSeen(new Date().getTime());
        }

        return cell;
    }

    private static String getTechnology(String input) {
        switch(input) {
            case "2G":
                return "GSM";
            case "3G":
                return "UMTS";
            case "4G":
                return "LTE";
            default:
                return input;
        }
    }

}
