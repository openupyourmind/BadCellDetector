/*
Copyright (c) 2015-* by agilob

ISC License - The acceptable one

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD
TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/
package org.badcoders.aimsicd.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.badcoders.aimsicd.Aimsicd;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;
import org.badcoders.aimsicd.utils.GeoLocation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RadioCellDownloadTask extends AsyncTask<Cell, Integer, Integer> {

    private static final String TAG = "RadioCelDownTask";

    private Context mContext;
    private DatabaseAdapter mDbAdapter;

    private String body;

    public RadioCellDownloadTask(Context context, DatabaseAdapter adapter) {
        this.mContext = context;
        this.mDbAdapter = adapter;
    }

    @Override
    protected Integer doInBackground(Cell... cell) {

        String urlLink = buildRequest(cell[0]);

        int code;
        try {
            HttpRequest request = HttpRequest.get(urlLink);
            code = request.code();
            this.body = request.body();
        } catch(RuntimeException re) {
            Log.e(TAG, "RunTimeException: " + re.getMessage(), re);
            return -1000;
        }
        return code;
    }

    private List<Cell> buildCellsFromJson(String json) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(json);
        JsonArray jsonArr = jo.getAsJsonArray("features");

        if(jsonArr.size() == 0)
            return Collections.emptyList();

        int size = jsonArr.size();

        Cell cellModel;
        List<Cell> cells = new ArrayList<>();

        for(int i = 0; i < size; i++) {
            cellModel = RadioCellHelper.getCellFromJson(jsonArr.get(i).getAsJsonObject());
            cells.add(cellModel);
        }

        return cells;
    }

    @Override
    protected void onPostExecute(Integer result) {
        if(result == 200) {
            Log.i(TAG, "Download region request returned 200 HTTP code");

            onProgressUpdate(60, 100);
            Toast.makeText(mContext, mContext.getString(
                    R.string.cell_data_successfully_received_markers_updated),
                           Toast.LENGTH_SHORT).show();

            List<Cell> cells = buildCellsFromJson(body);
            Log.i(TAG, "RadioCell found "  + cells.size() + " matching cells, inserting them to DB");
            for(int i = 0; i < cells.size(); i++) {
                mDbAdapter.insertImportedCell(cells.get(i), "RadioCell");
            }

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("cell_data_downloaded", true).apply();

            onProgressUpdate(100, 100);
        } else if(result == -1010101) {
            Toast.makeText(mContext, mContext.getString(R.string.unable_to_build_request),
                           Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(mContext, mContext.getString(
                    R.string.unknown_problem_connecting_to_online_resource_encountered) + " " + result,
                           Toast.LENGTH_SHORT).show();
        }

        onProgressUpdate(0, 100);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if(Aimsicd.mProgressBar != null) {
            Aimsicd.mProgressBar.setProgress(values[0]);
            Aimsicd.mProgressBar.setMax(values[1]);
        }
    }

    private String buildRequest(Cell cell) {
        Log.i(TAG, "Building RadioCell download region request");

        double earthRadius = 6371.01; // [km]
        int radius = 20; // Use 20 km radius with center at GPS location.

        //New GeoLocation object to find bounding Coordinates
        GeoLocation currentLoc = GeoLocation.fromDegrees(cell.getLat(), cell.getLon());

        //Calculate the Bounding Box Coordinates using an N km "radius"
        GeoLocation[] boundingCoords = currentLoc.boundingCoordinates(radius, earthRadius);
        //@formatter:off
        String boundParameter =
                "&north=" + Formatter.formatLocation(boundingCoords[0].getLatitudeInDegrees()) +
                "&west=" + Formatter.formatLocation(boundingCoords[0].getLongitudeInDegrees()) +
                "&south=" + Formatter.formatLocation(boundingCoords[1].getLatitudeInDegrees()) +
                "&east=" + Formatter.formatLocation(boundingCoords[1].getLongitudeInDegrees());
        //@formatter:on

        // this will be enabled in future when RadioCell becomes 90% reliable
        //String cellParameter = "network_code=" + cell.getMCC() + "-" + cell.getMNC();

        return "https://radiocells.org/api/cells_in_range?"
                                     // + cellParameter
                                     + boundParameter;
    }

}
