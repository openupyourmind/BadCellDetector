package org.badcoders.aimsicd.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.service.CellTracker;

/**
 * Get an API key for OpenCellID. Do not call this from the UI/Main thread.
 * For the various server responses, pleas refer to the OpenCellID API wiki:
 * http://wiki.opencellid.org/wiki/API#Error_codes
 * TODO: And the github issue #303:
 * https://github.com/SecUpwN/Android-IMSI-Catcher-Detector/issues/303
 * <p/>
 * OCID status codes http://wiki.opencellid.org/wiki/API#Error_codes
 * <ul>
 * <li>1    200    Cell not found</li>
 * <li>2    401    Invalid API key</li>
 * <li>3    400    Invalid input data</li>
 * <li>4    403    Your API key must be white listed in order to run this operation</li>
 * <li>5    500    Internal server error</li>
 * <li>6    503    Too many requests. Try later again</li>
 * <li>7    429    Daily limit 1000 requests exceeded for your API key</li>
 * </ul>
 */
public class OpenCellIdKeyDownloadTask extends AsyncTask<Void, Void, Integer> {

    private static final String TAG = "OpenCellIdKeyDownload";

    private Context mContext;
    private SharedPreferences mPreferences;
    private ProgressDialog mProgressDialog;
    private String response;

    public OpenCellIdKeyDownloadTask(Context context, SharedPreferences preferences, ProgressDialog progressDialog) {
        this.mContext = context;
        this.mPreferences = preferences;
        this.mProgressDialog = progressDialog;
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        HttpRequest request;
        try {
            request = HttpRequest.get(
                    mContext.getString(R.string.opencellid_api_get_key));
            response = request.body();
        } catch(RuntimeException ce) {
            return -1000; // no internet connection, eg. firewall is blocking app
        }
        return request.code();
    }

    @Override
    protected void onPostExecute(Integer responseCode) {
        String msg;
        switch(responseCode) {
            case 0:
                msg = mContext.getString(
                        R.string.error_getting_opencell_id_api_key) + ": " + response;
                break;
            case 200:
                // Check key validity (is done on foreign server)
                if(isKeyValid(response)) {
                    String ocidKey = mContext.getString(R.string.pref_ocid_key);
                    mPreferences.edit().putString(ocidKey, response).apply();
                    CellTracker.OCID_API_KEY = response;
                    msg = mContext.getString(R.string.ocid_api_success);
                } else {
                    msg = mContext.getString(R.string.invalid_api_key_format) + " " + response;
                }
                break;
            case 400:
                msg = "OCID Code 3: Invalid input data: " + response;
                Log.d(TAG, "OCID Code 3: Invalid input data: " + response);
                break;
            case 401:
                msg = "OCID Code 2: Invalid API Key: " + response;
                Log.d(TAG, "OCID Code 2: Invalid API Key: " + response);
                break;
            case 403:
                msg = "OCID Code 4:  Your API key must be white listed: " + response;
                Log.d(TAG, "OCID Code 4:  Your API key must be white listed: " + response);
                break;
            case 429:
                msg = "OCID Code 7: Exceeded daily request limit (1000) for your API key: " + response;
                Log.d(TAG,
                      "OCID Code 7: Exceeded daily request limit (1000) for your API key: " + response);
                break;
            case 500:
                msg = "OCID Code 5: Remote internal server error: " + response;
                Log.d(TAG, "OCID Code 5: Remote internal server error: " + response);
                break;
            case 503:
                msg = mContext.getString(R.string.only_one_key_per_day);
                Log.d(TAG, "OCID Code 6: Reached 24hr API key request limit: " + response);
                break;
            case -1000:
                msg = mContext.getString(
                        R.string.unknown_problem_connecting_to_online_resource_encountered);
                Log.d(TAG, "GOT -100 response code, probably firewall is blocking us");
                break;
            default:
                msg = "OCID returned unknown response code: " + responseCode;
                Log.e(TAG, "OCID Returned Unknown Response: " + responseCode);
                break;
        }
        mProgressDialog.dismiss();
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * This might be extended in the future.
     * Newly obtained keys start with: "dev-usr", not sure if that's a rule.
     */
    private boolean isKeyValid(String key) {
        return key.startsWith("dev-");
    }
}
