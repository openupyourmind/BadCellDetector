package org.badcoders.aimsicd.tasks;

import android.util.Log;

import org.badcoders.aimsicd.model.Cell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class OpenCellIdCellLookupParser {

    private static final String TAG = "OCIDLookupParser";

    public List<Cell> parse(File tmpFile) throws IOException {
        List<Cell> cells = new ArrayList<>();

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(tmpFile);

            doc.getDocumentElement().normalize();

            NodeList nodeList = doc.getElementsByTagName("rsp");

            for(int i = 0; i < nodeList.getLength(); i++) {
                Cell cell = new Cell();
                Node node = nodeList.item(i);
                Element fstElmnt = (Element) node;
                NodeList nameList = fstElmnt.getElementsByTagName("cell");
                Element nameElement = (Element) nameList.item(0);
                try {
                    cell.setLat(Double.parseDouble(nameElement.getAttribute("lat")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setLon(Double.parseDouble(nameElement.getAttribute("lon")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setMCC(Integer.parseInt(nameElement.getAttribute("mcc")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setMNC(Integer.parseInt(nameElement.getAttribute("mnc")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setLAC(Integer.parseInt(nameElement.getAttribute("lac")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setCellId(Integer.parseInt(nameElement.getAttribute("cellid")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setDBM(Integer.parseInt(nameElement.getAttribute("averageSignalStrength")));
                } catch(NumberFormatException nf) {}
                try {
                    cell.setSamples(Integer.parseInt(nameElement.getAttribute("samples")));
                } catch(NumberFormatException nf) {}
                cell.setTechnology(nameElement.getAttribute("radio"));
                try {
                    cell.setPSC(Integer.parseInt(nameElement.getAttribute("psc")));
                } catch(NumberFormatException nf) {}
                cells.add(cell);
            }
        } catch(ParserConfigurationException e) {
            Log.e(TAG, e.toString());
        } catch(SAXException e) {
            Log.e(TAG, e.toString());
        }
        return cells;
    }

}
