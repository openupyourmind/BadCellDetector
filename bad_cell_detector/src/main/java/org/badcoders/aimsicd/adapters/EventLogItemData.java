package org.badcoders.aimsicd.adapters;

/**
 * Class to show data of table "EventLog" in "Database Viewer"
 *
 * Issues:
 * [ ] See DatabaseAdapter.java line518
 * - How to find the right values (timestamp, lat, lan, accu) for saving in the db
 *
 *
 * Notes:
 *
 * We often talk about "Network Type", when we actually refer to:
 * "RAN" = Radio Access Network (cellular communaitcation only)
 * "RAT" = Radio Access Technology (any wireless communication technology, like WiMax etc.)
 *
 * As for this application, we shall use the terms:
 * "Type" for the text values like ( UMTS/WCDMA, HSDPA, CDMA, LTE etc)  and
 * "RAT" for the numerical equivalent (As obtained by AOS API?)
 *
 * ------------------------------------------------------------------------------------------
 */
public class EventLogItemData {

    // OLD (in old DB tables)
    private long mTimestamp;
    private String mCellID;
    private String mLac;
    private String mPsc;
    private String mLat;
    private String mLng;
    private String mgpsd_accu;
    private String mDF_id;
    private String mDF_desc;

    private final String mRecordId;

    public EventLogItemData(long pTime, String pLAC, String pCID, String pPSC, String pGpsd_lat,
                            String pGpsd_lon, String pGpsd_accu, String pDF_id, String pDF_desc,
                            String pRecordId) {
        this.mTimestamp = pTime;
        this.mLac = pLAC;
        this.mCellID = pCID;
        this.mPsc = pPSC;
        this.mLat = pGpsd_lat;
        this.mLng = pGpsd_lon;
        this.mgpsd_accu = pGpsd_accu;
        this.mDF_id = pDF_id;
        this.mDF_desc = pDF_desc;
        this.mRecordId = pRecordId;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    public String getCellID() {
        return mCellID;
    }

    public String getLac() {
        return mLac;
    }

    public String getPsc() {
        return mPsc;
    }

    public String getLat() {
        return mLat;
    }

    public String getLng() {
        return mLng;
    }

    public String getgpsd_accu() {
        return mgpsd_accu;
    }

    public String getDF_id() {
        return mDF_id;
    }

    public String getDF_desc() {
        return mDF_desc;
    }

    public String getRecordId() {
        return mRecordId;
    }
}
