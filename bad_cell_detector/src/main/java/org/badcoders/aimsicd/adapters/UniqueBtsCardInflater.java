package org.badcoders.aimsicd.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.Formatter;

public class UniqueBtsCardInflater implements IAdapterViewInflater<Cell> {

    @Override
    public View inflate(final BaseInflaterAdapter<Cell> adapter,
                        final int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.unique_bts_data, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Cell cell = adapter.getTItem(pos);
        holder.updateDisplay(cell, pos);

        return convertView;
    }

    private class ViewHolder {

        private final View mRootView;

        private final TextView cellId;
        private final TextView lac;
        private final TextView mcc;
        private final TextView mnc;
        private final TextView psc;

        private final TextView t3212;
        private final TextView stId;

        private final TextView timeFirst;
        private final TextView timeLast;
        private final TextView lat;
        private final TextView lon;

        private final TextView recordId;

        public ViewHolder(View rootView) {
            mRootView = rootView;

            lac = (TextView) mRootView.findViewById(R.id.lac);
            cellId = (TextView) mRootView.findViewById(R.id.cellid);
            mcc = (TextView) mRootView.findViewById(R.id.mcc);
            mnc = (TextView) mRootView.findViewById(R.id.mnc);
            psc = (TextView) mRootView.findViewById(R.id.psc);
            t3212 = (TextView) mRootView.findViewById(R.id.t3212);
            stId = (TextView) mRootView.findViewById(R.id.st_id);
            timeFirst = (TextView) mRootView.findViewById(R.id.first_seen);
            timeLast = (TextView) mRootView.findViewById(R.id.last_seen);
            lat = (TextView) mRootView.findViewById(R.id.lat);
            lon = (TextView) mRootView.findViewById(R.id.lon);

            recordId = (TextView) mRootView.findViewById(R.id.record_id);
            rootView.setTag(this);
        }

        public void updateDisplay(Cell cell, int counter) {

            cellId.setText(String.valueOf(cell.getCellId()));
            lac.setText(String.valueOf(cell.getLAC()));

            mcc.setText(String.valueOf(cell.getMCC()));
            mnc.setText(String.valueOf(cell.getMNC()));
            psc.setText(String.valueOf(cell.getPSC()));

            t3212.setText("n/a"); //t3212.setText(item.getT3212());
            stId.setText("n/a"); //stId.setText(item.getStId());

            timeFirst.setText(Formatter.formatDateTime(cell.getFirstSeen()));
            timeLast.setText(Formatter.formatDateTime(cell.getLastSeen()));

            lat.setText(Formatter.formatLocation(cell.getLat()));
            lon.setText(Formatter.formatLocation(cell.getLon()));

            recordId.setText(String.valueOf(counter));
        }
    }
}
