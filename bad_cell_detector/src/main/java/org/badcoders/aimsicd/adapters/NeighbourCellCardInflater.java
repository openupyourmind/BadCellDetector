package org.badcoders.aimsicd.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;

public class NeighbourCellCardInflater implements IAdapterViewInflater<Cell> {

    @Override
    public View inflate(final BaseInflaterAdapter<Cell> adapter,
                        final int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.cell_items, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Cell item = adapter.getTItem(pos);
        holder.updateDisplay(item, pos, adapter.getCount());

        return convertView;
    }

    private class ViewHolder {

        private final View mRootView;
        private final TextView mCellID;
        private final TextView mLac;
        private final TextView mMcc;
        private final TextView mMnc;
        private final TextView mNet;
        private final TextView rssi;
        private final TextView psc;
        private final TextView mRecordId;

        public ViewHolder(View rootView) {
            mRootView = rootView;
            mCellID = (TextView) mRootView.findViewById(R.id.cell_id);
            mLac = (TextView) mRootView.findViewById(R.id.lac);
            mMcc = (TextView) mRootView.findViewById(R.id.mcc);
            mMnc = (TextView) mRootView.findViewById(R.id.mnc);
            mNet = (TextView) mRootView.findViewById(R.id.net);
            rssi = (TextView) mRootView.findViewById(R.id.rssi);
            psc = (TextView) mRootView.findViewById(R.id.psc);
            mRecordId = (TextView) mRootView.findViewById(R.id.record_id);

            rootView.setTag(this);
        }

        public void updateDisplay(Cell cell, int record, int total) {
            mCellID.setText(String.valueOf(cell.getCellId()));
            mLac.setText(String.valueOf(cell.getLAC()));
            mNet.setText(String.valueOf(cell.getTechnology()));
            mMcc.setText(String.valueOf(cell.getMCC()));
            mMnc.setText(String.valueOf(cell.getMNC()));
            rssi.setText(String.valueOf(cell.getDBM()));
            psc.setText(String.valueOf(cell.getPSC()));
            mRecordId.setText((record + 1) + "/" + total);
        }
    }
}
