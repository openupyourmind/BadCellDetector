package org.badcoders.aimsicd.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import org.badcoders.aimsicd.Aimsicd;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.map.CellTowerMarker;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.service.CellTracker;
import org.badcoders.aimsicd.smsdetection.AdvanceUserItems;
import org.badcoders.aimsicd.smsdetection.CapturedSmsData;
import org.badcoders.aimsicd.smsdetection.DetectionStringsData;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class DatabaseAdapter extends SQLiteOpenHelper {

    private static final String COLUMN_id = "_id";
    private static final String COLUMN_cellId = "cellid";
    private static final String COLUMN_lac = "lac";
    private static final String COLUMN_lat = "lat";
    private static final String COLUMN_lon = "lon";
    private static final String COLUMN_mcc = "mcc";
    private static final String COLUMN_mnc = "mnc";
    private static final String COLUMN_dbm = "dbm";
    private static final String COLUMN_time = "time";
    private static final String COLUMN_tmsi = "tmsi";
    private static final String COLUMN_psc = "psc";
    private static final String COLUMN_speed = "speed";
    private static final String COLUMN_accuracy = "accuracy";
    private static final String COLUMN_technology = "technology";
    private static final String COLUMN_first_seen = "first_seen";
    private static final String COLUMN_last_seen = "last_seen";
    private static final String COLUMN_samples = "samples";
    private static final String COLUMN_avg_signal = "avg_signal";
    private static final String COLUMN_source = "source";
    private static final String COLUMN_string = "string";
    private static final String COLUMN_smsType = "smsType";
    private static final String COLUMN_isRoaming = "is_roaming";
    private static final String COLUMN_number = "number";
    private static final String COLUMN_smsc = "smsc";
    private static final String COLUMN_message = "message";
    private static final String COLUMN_type = "type";
    private static final String COLUMN_class = "class";

    private static final String TAG = "DbAdapter";

    private static final String DATABASE_NAME = "aimsicd";

    private static final int DATABASE_VERSION = 5;

    private static final String TABLE_OBSERVED = "observed_cells";
    private static final String TABLE_MEASURES = "cell_measures";
    private static final String TABLE_IMPORTS = "imported_cells";
    private static final String TABLE_EVENTLOG = "event_log";
    private static final String TABLE_SILENT_SMS = "silent_sms";
    private static final String TABLE_DETECTION_STRINGS = "detection_strings";

    //@formatter:off
    private static final String CREATE_TABLE_OBSERVED =
        "CREATE TABLE IF NOT EXISTS " + TABLE_OBSERVED + " (" +
            "_id            INTEGER PRIMARY KEY AUTOINCREMENT," +
            "cellid         INTEGER NOT NULL UNIQUE," +
            "lac            INTEGER NOT NULL," +
            "psc            INTEGER NOT NULL," +
            "mcc            INTEGER NOT NULL," +
            "mnc            INTEGER NOT NULL," +
            "technology     TEXT NOT NULL," +
            "avg_signal     INTEGER NOT NULL," +
            "first_seen     LONG NOT NULL," +
            "last_seen      LONG NOT NULL," +
            "lat            DOUBLE NOT NULL," +
            "lon            DOUBLE NOT NULL" +
        ");";

    private static final String CREATE_TABLE_MEASURES =
        "CREATE TABLE IF NOT EXISTS " + TABLE_MEASURES + " (" +
            "_id            INTEGER PRIMARY KEY AUTOINCREMENT," +
            "cellid         INTEGER NOT NULL," +
            "lac            INTEGER NOT NULL," +
            "psc            INTEGER NOT NULL," +
            "mcc            INTEGER NOT NULL," +
            "mnc            INTEGER NOT NULL," +
            "technology     TEXT NOT NULL," +
            "dbm            INTEGER NOT NULL," +
            "time           TEXT NOT NULL," +
            "lat            DOUBLE NOT NULL," +
            "lon            DOUBLE NOT NULL," +
            "tmsi           TEXT," +
            "speed          FLOAT NOT NULL," +
            "accuracy       FLOAT DEFAULT 0," +
            "is_submitted   INTEGER DEFAULT 0" +
        ");";

    private static final String CREATE_TABLE_IMPORTS =
        "CREATE TABLE IF NOT EXISTS " + TABLE_IMPORTS + " (" +
            "_id            INTEGER PRIMARY KEY AUTOINCREMENT," +
            "cellid         INTEGER NOT NULL," +
            "lac            INTEGER NOT NULL," +
            "psc            INTEGER NOT NULL," +
            "mcc            INTEGER NOT NULL," +
            "mnc            INTEGER NOT NULL," +
            "technology     TEXT NOT NULL," +
            "dbm            INTEGER NOT NULL," +
            "first_seen     LONG NOT NULL," +
            "last_seen      LONG NOT NULL, " +
            "lat            DOUBLE NOT NULL," +
            "lon            DOUBLE NOT NULL," +
            "samples        INTEGER NOT NULL," +
            "avg_signal     INTEGER NOT NULL," +
            "source         TEXT NOT NULL" +
        ");";

    private static final String CREATE_TABLE_SILENT_SMS =
        "CREATE TABLE IF NOT EXISTS " + TABLE_SILENT_SMS + " (" +
            "_id            INTEGER PRIMARY KEY AUTOINCREMENT," +
            "cellid         INTEGER NOT NULL," +
            "lac            INTEGER NOT NULL," +
            "technology     TEXT NOT NULL," +
            "lat            DOUBLE," +
            "lon            DOUBLE," +
            "time           TEXT," +
            "address        TEXT," +
            "display        TEXT," +
            "class          TEXT," +
            "type           TEXT," +
            "is_roaming     BOOLEAN," +
            "smsc           TEXT," +
            "message        TEXT," +
            "number         TEXT"+
        ");";

    private static final String CREATE_TABLE_EVENTLOG =
        "CREATE TABLE IF NOT EXISTS " + TABLE_EVENTLOG + " (" +
            "_id             INTEGER PRIMARY KEY AUTOINCREMENT," +
            "time            TEXT NOT NULL," +
            "LAC             INTEGER NOT NULL," +
            "cellid          INTEGER NOT NULL," +
            "PSC             INTEGER," +
            "lat             TEXT," +
            "lon             TEXT," +
            "accuracy        INTEGER," +
            "DF_id           INTEGER," +
            "DF_description  TEXT" +
        ");";

    private static final String CREATE_TABLE_DETECTION_STRINGS =
        "CREATE TABLE IF NOT EXISTS " + TABLE_DETECTION_STRINGS + " (" +
            "_id            INTEGER PRIMARY KEY AUTOINCREMENT," +
            "string         TEXT NOT NULL," +
            "smsType        TEXT NOT NULL" +
        ");";
    //@formatter:on

    private Context mContext;

    public DatabaseAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_OBSERVED);
        db.execSQL(CREATE_TABLE_MEASURES);
        db.execSQL(CREATE_TABLE_IMPORTS);
        db.execSQL(CREATE_TABLE_EVENTLOG);
        db.execSQL(CREATE_TABLE_DETECTION_STRINGS);
        insertInitialDetectionStrings(db);
        db.execSQL(CREATE_TABLE_SILENT_SMS);
    }

    private void insertInitialDetectionStrings(SQLiteDatabase db) {
        Map<String, String> detectionMap = new HashMap<>();
        detectionMap.put("Received short message type 0", "TYPE0");
        detectionMap.put("Received voice mail indicator clear SMS shouldStore=false", "MWI");
        detectionMap.put("SMS TP-PID:0 data coding scheme: 24", "FLASH");
        detectionMap.put("isTypeZero=true", "TYPE0");
        detectionMap.put("incoming msg. Mti 0 ProtocolID 0 DCS 0x04 class -1", "WAPPUSH");
        detectionMap.put("SMS TP-PID:0 data coding scheme: 4", "WAPPUSH");

        ContentValues detectionStrings;
        for(String key : detectionMap.keySet()) {
            detectionStrings = new ContentValues();
            detectionStrings.put(COLUMN_string, key);
            detectionStrings.put(COLUMN_smsType, detectionMap.get(key));
            db.insert(TABLE_DETECTION_STRINGS, null, detectionStrings);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                           + newVersion + ", which will destroy all old data");
        dropTables(db);
        onCreate(db);
    }

    public void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEASURES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMPORTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMPORTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SILENT_SMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETECTION_STRINGS);
    }

    public long insertCellMeasure(Cell cell) {
        if(cell.getCellId() == -1 || cell.getLAC() == -1 ||
                   cell.getLat() == 0.0d || cell.getLon() == 0.0d) {
            return -1l;
        } else {
            Log.d(TAG, "Inserting measure cell to database: " + cell.getCellId());
            ContentValues values = new ContentValues();
            values.put(COLUMN_cellId, cell.getCellId());
            values.put(COLUMN_lac, cell.getLAC());
            values.put(COLUMN_lat, cell.getLat());
            values.put(COLUMN_lon, cell.getLon());
            values.put(COLUMN_mcc, cell.getMCC());
            values.put(COLUMN_mnc, cell.getMNC());
            values.put(COLUMN_dbm, cell.getDBM());
            values.put(COLUMN_time, new Date().getTime());
            values.put(COLUMN_tmsi, cell.getTmsi());
            values.put(COLUMN_psc, cell.getPSC());
            values.put(COLUMN_technology, cell.getTechnology());
            values.put(COLUMN_speed, cell.getSpeed());
            values.put(COLUMN_accuracy, cell.getAccuracy());

            return getWritableDatabase().insert(TABLE_MEASURES, null, values);
        }
    }

    public long insertImportedCell(Cell cell, String source) {
        if(cell.getCellId() == -1 || cell.getLAC() == -1 ||
                   cell.getLat() == 0.0d || cell.getLon() == 0.0d) {
            return -1l;
        } else {
            Log.d(TAG, "Inserting imported cell to database: " + cell.getCellId());
            ContentValues values = new ContentValues();
            values.put(COLUMN_cellId, cell.getCellId());
            values.put(COLUMN_lac, cell.getLAC());
            values.put(COLUMN_psc, cell.getPSC());
            values.put(COLUMN_mcc, cell.getMCC());
            values.put(COLUMN_mnc, cell.getMNC());
            values.put(COLUMN_technology, cell.getTechnology());
            values.put(COLUMN_dbm, cell.getDBM());
            values.put(COLUMN_first_seen, cell.getFirstSeen());
            values.put(COLUMN_last_seen, cell.getLastSeen());
            values.put(COLUMN_lat, cell.getLat());
            values.put(COLUMN_lon, cell.getLon());
            values.put(COLUMN_samples, cell.getSamples());
            values.put(COLUMN_avg_signal, cell.getDBM());

            values.put(COLUMN_source, source);

            return getWritableDatabase().insert(TABLE_IMPORTS, null, values);
        }
    }

    public List<Cell> getAllCellMeasures() {
        List<Cell> cells = new ArrayList<>();
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_MEASURES,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_dbm ,
                COLUMN_time,
                COLUMN_tmsi,
                COLUMN_psc,
                COLUMN_technology,
                COLUMN_speed,
                COLUMN_accuracy},
                null, null, null, null, null, null);
        //@formatter:on
        if(cursor.getCount() == 0) {
            return cells;
        } else {
            while(cursor.moveToNext()) {
                cells.add(measureCursorToCell(cursor));
            }
        }

        return cells;
    }

    public List<Cell> getAllImportedCells() {
        List<Cell> cells = new ArrayList<>();
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_IMPORTS,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_psc,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_technology,
                COLUMN_dbm,
                COLUMN_first_seen,
                COLUMN_last_seen,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_samples,
                COLUMN_avg_signal,
                COLUMN_source},
                null, null, null, null, null, null);
        //@formatter:on
        if(cursor.getCount() == 0) {
            return cells;
        } else {
            while(cursor.moveToNext()) {
                cells.add(importedCursorToCell(cursor));
            }
        }

        return cells;
    }

    public List<Cell> getAllCellMeasuresForCell(int cellId) {
        List<Cell> cells = new ArrayList<>();
        //@formatter:off
            Cursor cursor = getWritableDatabase().query(TABLE_MEASURES,
                new String[] {
                    COLUMN_cellId,
                    COLUMN_lac,
                    COLUMN_lat,
                    COLUMN_lon,
                    COLUMN_mcc,
                    COLUMN_mnc,
                    COLUMN_dbm ,
                    COLUMN_time,
                    COLUMN_tmsi,
                    COLUMN_psc,
                    COLUMN_technology,
                    COLUMN_speed,
                    COLUMN_accuracy},
                    COLUMN_cellId + "=" + cellId,
                    null, null, null, null, null);
            //@formatter:on
        if(cursor.getCount() == 0) {
            return cells;
        } else {
            while(cursor.moveToNext()) {
                cells.add(measureCursorToCell(cursor));
            }
        }

        return cells;
    }

    public Cell getMeasureCellByCellId(long cid) {
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_MEASURES,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_dbm ,
                COLUMN_time,
                COLUMN_tmsi,
                COLUMN_psc,
                COLUMN_technology,
                COLUMN_speed,
                COLUMN_accuracy},
                COLUMN_cellId + "=" + cid,
                null, null, null, null, null);
        //@formatter:on

        if(cursor.getCount() == 0) {
            return new Cell();
        } else {
            cursor.moveToFirst();
            return measureCursorToCell(cursor);
        }
    }

    public void deleteMeasureByCellId(int cellid) {
        getWritableDatabase().delete(TABLE_MEASURES, COLUMN_cellId + " = " + cellid,
                                     null);
    }

    public static Cell measureCursorToCell(Cursor cursor) {
        Cell cell = new Cell();
        cell.setCellId(cursor.getInt(0));
        cell.setLAC(cursor.getInt(1));
        cell.setLat(cursor.getDouble(2));
        cell.setLon(cursor.getDouble(3));
        cell.setMCC(cursor.getInt(4));
        cell.setMNC(cursor.getInt(5));
        cell.setDBM(cursor.getInt(6));
        cell.setLastSeen(cursor.getLong(7));
        cell.setTmsi(cursor.getString(8));
        cell.setPSC(cursor.getInt(9));
        cell.setTechnology(cursor.getString(10));
        cell.setSpeed(cursor.getFloat(11));
        cell.setAccuracy(cursor.getFloat(12));
        return cell;
    }

    public static Cell importedCursorToCell(Cursor cursor) {
        Cell cell = new Cell();
        cell.setCellId(cursor.getInt(0));
        cell.setLAC(cursor.getInt(1));
        cell.setPSC(cursor.getInt(2));
        cell.setMCC(cursor.getInt(3));
        cell.setMNC(cursor.getInt(4));
        cell.setTechnology(cursor.getString(5));
        cell.setDBM(cursor.getInt(6));
        cell.setFirstSeen(cursor.getLong(7));
        cell.setLastSeen(cursor.getLong(8));
        cell.setLat(cursor.getDouble(9));
        cell.setLon(cursor.getDouble(10));
        cell.setSamples(cursor.getInt(11));
        cell.setDBM(cursor.getInt(12));
        cell.setSource(cursor.getString(13));
        return cell;
    }

    public static Cell observedToCell(Cursor cursor) {
        Cell cell = new Cell();
        cell.setCellId(cursor.getInt(0));
        cell.setLAC(cursor.getInt(1));
        cell.setPSC(cursor.getInt(2));
        cell.setMCC(cursor.getInt(3));
        cell.setMNC(cursor.getInt(4));
        cell.setTechnology(cursor.getString(5));
        cell.setDBM(cursor.getInt(6));
        cell.setFirstSeen(cursor.getLong(7));
        cell.setLastSeen(cursor.getLong(8));
        cell.setLat(cursor.getDouble(9));
        cell.setLon(cursor.getDouble(10));
        return cell;
    }

    public static DetectionStringsData detectionStringToDetectionStringsData(Cursor cursor) {
        return new DetectionStringsData(cursor.getString(1), cursor.getString(2));
    }

    private CellTowerMarker importedCursorToCellTowerMarker(MapView mMap, Cursor cursor) {
        return new CellTowerMarker(mContext, mMap,
                                   mContext.getString(R.string.cell_id) + cursor.getInt(0),
                                   new GeoPoint(cursor.getDouble(9), cursor.getDouble(10)),
                                   measureCursorToCell(cursor));
    }

    public Cursor getOCIDSubmitData() {
        String query = "SELECT DISTINCT MCC,MNC,LAC,cellid,lon,lat,rx_signal,time,accuracy FROM DBi_measure, DBi_bts WHERE isSubmitted <> 1 ORDER BY time;";
        return getWritableDatabase().rawQuery(query, null);
    }

    public Cell getImportedCell(int cellid) {

        Cursor cursor = getWritableDatabase().query(TABLE_IMPORTS, //@formatter:off
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_psc,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_technology,
                COLUMN_dbm,
                COLUMN_first_seen,
                COLUMN_last_seen,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_samples,
                COLUMN_avg_signal,
                COLUMN_source},
                COLUMN_cellId + "=" + cellid,
                null, null, null, null, null);
        //@formatter:on
        if(cursor.getCount() == 0) {
            return new Cell();
        } else {
            cursor.moveToFirst();
            return importedCursorToCell(cursor);
        }
    }

    /**
     * This take a "Cell" bundle (from API) as input and uses its CID to check
     * in the DBi_measure (?) if there is already an associated LAC. It then
     * compares the API LAC to that of the DBi_Measure LAC.
     * <p/>
     * Issues:     [ ] We should make all detections outside of DatabaseAdapter.java in a
     * separate module as described in the diagram in GH issue #215.
     * https://github.com/SecUpwN/Android-IMSI-Catcher-Detector/issues/215
     * where it is referred to as "Detection Module" (DET)...
     * <p/>
     * This is using the LAC found by API and comparing to LAC found from a previous
     * measurement in the "DBi_measure". This is NOT depending on "DBe_import".
     * This works for now...but we probably should consider populating "DBi_measure"
     * as soon as the API gets a new LAC. Then the detection can be done by SQL,
     * and by just comparing last 2 LAC entries for same CID.
     */
    public boolean checkLAC(Cell cell) {

        List<Cell> cells = getAllCellMeasuresForCell(cell.getCellId());
        Cell imported = getImportedCell(cell.getCellId());
        if(imported.getCellId() != -1)
            cells.add(imported);

        if(cells.isEmpty()) {
            return true;
        }
        boolean contains = false;

        for(Cell c : cells) {
            if(c.getLAC() == cell.getLAC()) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    public List<CellTowerMarker> getImportedCellTowerMarkers(MapView mMap, int mcc, int mnc) {
        List<CellTowerMarker> towerMarkers = new ArrayList<>();

        //@formatter:off
       Cursor cursor = getWritableDatabase().query(TABLE_IMPORTS,
           new String[] {
               COLUMN_cellId,
               COLUMN_lac,
               COLUMN_psc,
               COLUMN_mcc,
               COLUMN_mnc,
               COLUMN_technology,
               COLUMN_dbm,
               COLUMN_first_seen,
               COLUMN_last_seen,
               COLUMN_lat,
               COLUMN_lon,
               COLUMN_samples,
               COLUMN_avg_signal,
               COLUMN_source},
               COLUMN_mnc + "=" + mnc + " AND " + COLUMN_mcc + "=" + mcc,
               null, null, null, null);
       //@formatter:on
        while(cursor.moveToNext()) {
            towerMarkers.add(importedCursorToCellTowerMarker(mMap, cursor));
        }

        List<Cell> cells = getAllCellMeasures();
        for(Cell cell : cells) {
            towerMarkers.add(new CellTowerMarker(mContext, mMap,
                                                 mContext.getString(
                                                         R.string.cell_id) + cell.getCellId(),
                                                 new GeoPoint(cell.getLat(), cell.getLon()),
                                                 cell));
        }

        return towerMarkers;
    }


    public List<Cell> getImportedCellsByNetwork(int mcc, int mnc) {
        List<Cell> cells = new ArrayList<>();

        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_IMPORTS,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_psc,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_technology,
                COLUMN_dbm,
                COLUMN_first_seen,
                COLUMN_last_seen,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_samples,
                COLUMN_avg_signal,
                COLUMN_source},
                COLUMN_mnc + "=" + mnc + " AND " + COLUMN_mcc + "=" + mcc,
                null, null, null, null);
        //@formatter:on
        while(cursor.moveToNext()) {
            cells.add(importedCursorToCell(cursor));
        }
        return cells;
    }

    public boolean prepareOpenCellUploadData() {
        boolean result;

        File file;
        try {
            file = File.createTempFile("aimsicd-ocid-data", "csv");
        } catch(IOException e) {
            Log.e(TAG, e.getMessage());
            return false;
        }

        try {
            // Get data not yet submitted:
            Cursor c = getOCIDSubmitData();
            // Check if we have something to upload:
            if(c.getCount() > 0) {
                if(!file.exists()) {
                    result = file.createNewFile();
                    if(!result) {
                        c.close();
                        return false;
                    }

                    // OCID CSV upload format and items
                    // mcc,mnc,lac,cellid,lon,lat,signal,measured_at,rating,speed,direction,act,ta,psc,tac,pci,sid,nid,bid
                    CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                    csvWrite.writeNext("mcc,mnc,lac,cellid,lon,lat,signal,measured_at,rating");

                    int size = c.getCount();
                    Log.d(TAG, "OCID UPLOAD: row count = " + size);

                    while(c.moveToNext()) {
                        csvWrite.writeNext(
                            String.valueOf(c.getInt(c.getColumnIndex("MCC"))),
                            String.valueOf(c.getInt(c.getColumnIndex("MNC"))),
                            String.valueOf(c.getInt(c.getColumnIndex("LAC"))),
                            String.valueOf(c.getInt(c.getColumnIndex("CID"))),
                            c.getString(c.getColumnIndex("lon")),
                            c.getString(c.getColumnIndex("lat")),
                            c.getString(c.getColumnIndex("rx_signal")),
                            c.getString(c.getColumnIndex("time")),
                            //c.getString(c.getColumnIndex("RAT")),
                            String.valueOf(c.getInt(c.getColumnIndex("accuracy"))));
                    }
                    csvWrite.close();
                    c.close();
                }
                return true;
            }
            c.close();
            return false;
        } catch(Exception e) {
            Log.e(TAG, "prepareOpenCellUploadData(): Error creating OpenCellID Upload Data: ", e);
            return false;
        } finally {
            if(file != null) {
                file.deleteOnExit();
            }
        }
    }

    public boolean populateDBeImport(String source) {
        File file = new File(source);

        try {
            if(file.exists()) {

                CSVReader csvReader = new CSVReader(new FileReader(file));
                List<String[]> csvCellID = new ArrayList<>();
                String next[];

                // Let's show something: Like 1/4 of a progress bar
                Aimsicd.mProgressBar.setProgress(0);
                Aimsicd.mProgressBar.setMax(4);
                Aimsicd.mProgressBar.setProgress(1);

                while((next = csvReader.readNext()) != null) {
                    csvCellID.add(next);
                }

                Aimsicd.mProgressBar.setProgress(2);

                if(!csvCellID.isEmpty()) {
                    int lines = csvCellID.size();
                    Log.i(TAG, "UpdateOpenCellID: OCID CSV size (lines): " + lines);

                    Cursor lCursor = getWritableDatabase().rawQuery(
                            "SELECT cellid, COUNT(cellid) FROM DBe_import GROUP BY cellid", null);
                    SparseArray<Boolean> lPresentCellID = new SparseArray<>();
                    if(lCursor.getCount() > 0) {
                        while(lCursor.moveToNext()) {
                            lPresentCellID.put(lCursor.getInt(0), true);
                        }
                    }
                    lCursor.close();

                    Aimsicd.mProgressBar.setProgress(3);
                    Aimsicd.mProgressBar.setMax(lines);

                    int rowCounter;
                    for(rowCounter = 1; rowCounter < lines; rowCounter++) {
                        // Inserted into the table only unique values CID
                        // without opening additional redundant cursor before each insert.
                        if(lPresentCellID.get(Integer.parseInt(csvCellID.get(rowCounter)[5]),
                                              false)) {
                            continue;
                        }
                        // Insert details into OpenCellID Database using:  insertDBeImport()
                        // Beware of negative values of "range" and "samples"!!
                        String lat = csvCellID.get(rowCounter)[0],           //TEXT
                                lon = csvCellID.get(rowCounter)[1],          //TEXT
                                mcc = csvCellID.get(rowCounter)[2],          //int
                                mnc = csvCellID.get(rowCounter)[3],          //int
                                lac = csvCellID.get(rowCounter)[4],          //int
                                cellid = csvCellID.get(rowCounter)[5],     //int   long CID [>65535]
                                range = csvCellID.get(rowCounter)[6],        //int
                                avg_sig = csvCellID.get(rowCounter)[7],      //int
                                samples = csvCellID.get(rowCounter)[8],      //int
                                change = csvCellID.get(rowCounter)[9],       //int
                                radio = csvCellID.get(rowCounter)[10],       //TEXT
                                rnc = csvCellID.get(rowCounter)[11],         //int
                                cid = csvCellID.get(rowCounter)[12],      //int   short CID [<65536]
                                psc = csvCellID.get(rowCounter)[13];         //int

                        // TODO: WHAT IS THIS DOING? Can we remove?
                        // (There shouldn't be any bad PSCs in the import...)
                        int iPsc = 0;
                        if(psc != null && !psc.isEmpty()) {
                            iPsc = Integer.parseInt(psc);
                        }

                        //Reverse order 1 = 0 & 0 = 1
                        // what if ichange is 4? ~ agilob
                        int ichange = Integer.parseInt(change);
                        ichange = (ichange == 0 ? 1 : 0);

                        insertDBeImport(
                            "OCID",                     // DBsource
                            radio,                      // RAT
                            Integer.parseInt(mcc),      // MCC
                            Integer.parseInt(mnc),      // MNC
                            Integer.parseInt(lac),      // LAC
                            Integer.parseInt(cellid),   // CID (cellid) ?
                            iPsc,                       // psc
                            lat,                        // gps_lat
                            lon,                        // gps_lon
                            ichange,                    // isGPSexact
                            Integer.parseInt(avg_sig),  // avg_signal [dBm]
                            Integer.parseInt(range),    // avg_range [m]
                            Integer.parseInt(samples),  // samples
                            "n/a",                      // time_first  (not in OCID)
                            "n/a",                      // time_last   (not in OCID)
                            0                           // TODO: rej_cause , set default 0
                        );
                    }
                    Aimsicd.mProgressBar.setProgress(4);
                    Log.d(TAG, "PopulateDBeImport(): inserted " + rowCounter + " cells.");
                }
            } else {
                Log.e(TAG, "Opencellid.csv file does not exist!");
            }
            return true;
        } catch(Exception e) {
            Log.e(TAG, "Error parsing OpenCellID data: " + e.getMessage(), e);
            return false;
        } finally {
            try {
                Thread.sleep(1000); // wait 1 second to allow user to see progress bar.
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            Aimsicd.mProgressBar.setProgress(0);
        }
    }

    public List<AdvanceUserItems> getDetectionStrings() {

        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM " + TABLE_DETECTION_STRINGS,
                                                       null);

        List<AdvanceUserItems> detectionStrings = new ArrayList<>();

        while(cursor.moveToNext()) {
            AdvanceUserItems setItems = new AdvanceUserItems();
            setItems.setDetectionString(cursor.getString(cursor.getColumnIndex(COLUMN_string)));
            setItems.setDetectionType(cursor.getString(cursor.getColumnIndex(COLUMN_smsType)));
            detectionStrings.add(setItems);
        }
        cursor.close();
        return detectionStrings;
    }

    public boolean deleteDetectedSms(long deleteme) {
        try {
            getWritableDatabase().delete(TABLE_SILENT_SMS, "_id=" + deleteme, null);
            return true;
        } catch(Exception ee) {
            Log.i(TAG, "Deleting SMS data failed", ee);
        }
        return false;
    }

    public boolean deleteDetectionString(String deleteme) {
        try {
            getWritableDatabase().delete(TABLE_DETECTION_STRINGS, COLUMN_string + "=" + deleteme,
                                         null);
            return true;
        } catch(Exception ee) {
            Log.i(TAG, "Deleting detection string failed", ee);
        }
        return false;

    }

    public boolean insertNewDetectionString(ContentValues newString) {

        // First check that string not in DB
        String check4String = String.format(
                "SELECT * FROM " + TABLE_DETECTION_STRINGS + " WHERE " + COLUMN_string + " = \"%s\"",
                newString.get(COLUMN_string).toString());

        Cursor cursor = getWritableDatabase().rawQuery(check4String, null);
        final boolean exists = cursor.getCount() > 0;
        cursor.close();

        if(exists) {
            Log.i(TAG, "Detection String already in Database");
        } else {
            try {
                getWritableDatabase().insert(TABLE_DETECTION_STRINGS, null, newString);
                Log.i(TAG, "New detection string added.");
                return true;
            } catch(Exception ee) {
                Toast.makeText(mContext, ee.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Adding detection string Failed! ", ee);
                return false;
            }
        }
        return false;
    }

    public CapturedSmsData storeCapturedSms(CapturedSmsData smsdata) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_number, smsdata.getSenderNumber());
        values.put(COLUMN_message, smsdata.getSenderMsg());
        values.put(COLUMN_time, smsdata.getSmsTimestamp());
        values.put(COLUMN_type, smsdata.getSmsType());
        values.put(COLUMN_lac, smsdata.getLAC());
        values.put(COLUMN_cellId, smsdata.getCellId());
        values.put(COLUMN_technology, smsdata.getTechnology());
        values.put(COLUMN_isRoaming, smsdata.getRoamingStatus());
        values.put(COLUMN_lat, smsdata.getLat());
        values.put(COLUMN_lon, smsdata.getLon());

        long insertId = getWritableDatabase().insert(TABLE_SILENT_SMS, null, values);
        smsdata.setId(insertId);
        return smsdata;
    }

    // Boolean Check if a give timestamp already exists in SmsData table
    public boolean isTimeStampInDB(Long timestamp) {
        String check4timestamp = String.format(
                "SELECT time FROM " + TABLE_SILENT_SMS + " WHERE time = \"%s\"", timestamp);
        Cursor cursor = getWritableDatabase().rawQuery(check4timestamp, null);
        final boolean exists = cursor.getCount() > 0;
        cursor.close();
        return exists;
    }

    public Cursor returnEventLogData() {
        return getWritableDatabase().rawQuery("SELECT * FROM " + TABLE_EVENTLOG, null);
    }

    public Cursor returnDetectionStrings() {
        return getWritableDatabase().rawQuery("SELECT * FROM " + TABLE_DETECTION_STRINGS, null);
    }

    public Cursor returnSmsData() {
        return getWritableDatabase().rawQuery("SELECT * FROM " + TABLE_SILENT_SMS, null);
    }

    /**
     * This method is used to insert and populate the downloaded or previously
     * backed up OCID details into the DBe_import database table.
     * <p/>
     * It also prevents adding multiple entries of the same cell-id, when OCID
     * downloads are repeated.
     */
    public void insertDBeImport(String db_src, String rat, int mcc, int mnc, int lac,
                                int cid, int psc, String lat, String lon, int isGpsExact,
                                int avg_range, int avg_signal, int samples, String time_first,
                                String time_last, int rej_cause) {
        ContentValues dbeImport = new ContentValues();

        dbeImport.put("DBsource", db_src);
        dbeImport.put("RAT", rat);
        dbeImport.put("MCC", mcc);
        dbeImport.put("MNC", mnc);
        dbeImport.put("LAC", lac);
        dbeImport.put("cellid", cid);
        dbeImport.put("PSC", psc);
        dbeImport.put("gps_lat", lat);
        dbeImport.put("gps_lon", lon);
        dbeImport.put("isGPSexact", isGpsExact);
        dbeImport.put("avg_range", avg_range);
        dbeImport.put("avg_signal", avg_signal);
        dbeImport.put("samples", samples);
        dbeImport.put("time_first", time_first);
        dbeImport.put("time_last", time_last);
        dbeImport.put("rej_cause", rej_cause);

        // Check that the LAC/CID is not already in the DBe_import (to avoid adding duplicate cells)
        String query = String.format(
                "SELECT LAC,cellid FROM DBe_import WHERE LAC = %d AND cellid = %d ",
                lac, cid);
        Cursor cursor = getWritableDatabase().rawQuery(query, null);
        if(cursor.getCount() < 1) { // < 1 means cell is not in database yet
            getWritableDatabase().insert("DBe_import", null, dbeImport);
        }
        cursor.close();
    }

    public boolean isObserved(int cellid, int lac) {
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_OBSERVED,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac},
                COLUMN_cellId + "=" + cellid + " AND " + COLUMN_lac + "=" + lac,
                null, null, null, null);
        //@formatter:on
        return cursor.getCount() < 1;
    }

    public Cell getObserved(int cellid, int lac) {
    //@formatter:off
    Cursor cursor = getWritableDatabase().query(TABLE_OBSERVED,
        new String[] {
            COLUMN_cellId,
            COLUMN_lac,
            COLUMN_psc,
            COLUMN_mcc,
            COLUMN_mnc,
            COLUMN_technology,
            COLUMN_avg_signal,
            COLUMN_first_seen,
            COLUMN_last_seen,
            COLUMN_lat,
            COLUMN_lon},
            COLUMN_cellId + "=" + cellid + " AND " + COLUMN_lac + "=" + lac,
            null, null, null, null);
        //@formatter:on
        if(cursor.getCount() > 0) {
            cursor.moveToNext();
            return observedToCell(cursor);
        } else {
            return new Cell();
        }
    }

    private void insertObserved(Cell cell) {
        if(cell.getCellId() == -1 || cell.getLAC() == -1) {
            return;
        }
        Log.d(TAG, "Inserting new observed cell to database: " + cell.getCellId());
        ContentValues values = new ContentValues();
        values.put(COLUMN_cellId, cell.getCellId());
        values.put(COLUMN_lac, cell.getLAC());
        values.put(COLUMN_psc, cell.getPSC());
        values.put(COLUMN_mcc, cell.getMCC());
        values.put(COLUMN_mnc, cell.getMNC());
        values.put(COLUMN_technology, cell.getTechnology());
        values.put(COLUMN_avg_signal, cell.getDBM());
        values.put(COLUMN_first_seen, new Date().getTime());
        values.put(COLUMN_last_seen, new Date().getTime());
        values.put(COLUMN_lat, cell.getLat());
        values.put(COLUMN_lon, cell.getLon());
        getWritableDatabase().insert(TABLE_OBSERVED, null, values);
    }

    public void updateObserved(Cell cell) {
        if(cell.getCellId() == -1 || cell.getLAC() == -1) {
            return;
        }
        if(!isObserved(cell.getCellId(), cell.getLAC())) {
            insertObserved(cell);
            return;
        }
        Log.d(TAG, "Updating existing observed cell: " + cell.getCellId());
        ContentValues values = new ContentValues();
        values.put(COLUMN_avg_signal, cell.getDBM());
        values.put(COLUMN_last_seen, new Date().getTime());
        if(cell.getLat() != 0.0d && cell.getLon() != 0.0d) {
            values.put(COLUMN_lat, cell.getLat());
            values.put(COLUMN_lon, cell.getLon());
        }
        getWritableDatabase().update(TABLE_OBSERVED, values,
                                     COLUMN_cellId + "=" + cell.getCellId() + " AND " + COLUMN_lac + "= "+ cell.getLAC(),
                                     null);
    }

    /**
     * Created this because we don't need to insert all the data in this table
     * since we don't yet have items like TMSI etc.
     */
    public void insertBTS(Cell cell) {
        if(cell.getCellId() == -1)
            return;
        Cell observed = getObserved(cell.getCellId(), cell.getLAC());

        // If LAC and CID are not already in DBi_bts, then add them.
        if(observed.getCellId() == -1) { //it's not created by `new`
            insertObserved(cell);
        }
    }

    /**
     * This inserts Detection Flag data that is used to fine tune the various
     * available detection mechanisms. (See Detection List in issue #230)
     * <p/>
     * These parameters are described elsewhere, but should be included here
     * for completeness and reference.
     * <p/>
     * There's also a CounterMeasure id to link to possible future counter measures.
     */
    public void insertDetectionFlags(int code, String name, String description, int p1, int p2, int p3,
                                     double p1_fine, double p2_fine, double p3_fine,
                                     String app_text, String func_use, int istatus, int CM_id) {

        ContentValues detectionFlags = new ContentValues();
        detectionFlags.put("code", code);
        detectionFlags.put("name", name);
        detectionFlags.put("description", description);
        detectionFlags.put("p1", p1);
        detectionFlags.put("p2", p2);
        detectionFlags.put("p3", p3);
        detectionFlags.put("p1_fine", p1_fine);
        detectionFlags.put("p2_fine", p2_fine);
        detectionFlags.put("p3_fine", p3_fine);
        detectionFlags.put("app_text", app_text);
        detectionFlags.put("func_use", func_use);
        detectionFlags.put("istatus", istatus);
        detectionFlags.put("CM_id", CM_id);

        getWritableDatabase().insert("DetectionFlags", null, detectionFlags);
    }

    public void insertEventLog(String time, int lac, int cid, int psc, String lat,
                               String lon, int accuracy, int DF_id, String DF_description) {

        if(cid != -1) { // skip CID of "-1" (due to crappy API or roaming or Air-Plane Mode)
            // ONLY check if LAST entry is the same!
            String query = String.format(
                    // "SELECT * FROM EventLog WHERE LAC=%d AND CID=%d AND DF_id=%d ORDER BY _id DESC LIMIT 1",
                    "SELECT * from " + TABLE_EVENTLOG + " WHERE _id=(SELECT max(_id) from EventLog) AND cellid=%d AND LAC=%d AND DF_id=%d",
                    // was: "SELECT * FROM EventLog WHERE CID = %d AND LAC = %d AND DF_id = %d",
                    cid, lac, DF_id);
            Cursor cursor = getWritableDatabase().rawQuery(query, null);

            boolean insertData = true;
            if(cursor.getCount() > 0) {
                insertData = false;
            }
            cursor.close();

            if(insertData) {
                ContentValues eventLog = new ContentValues();

                eventLog.put("time", time);
                eventLog.put("LAC", lac);
                eventLog.put("cellid", cid);
                eventLog.put("PSC", psc);
                eventLog.put("lat", lat);
                eventLog.put("lon", lon);
                eventLog.put("accuracy", accuracy);
                eventLog.put("DF_id", DF_id);
                eventLog.put("DF_description", DF_description);

                getWritableDatabase().insert(TABLE_EVENTLOG, null, eventLog);
                Log.i(TAG, "InsertEventLog(): Insert detection event into EventLog table with cellid=" + cid);
            }
        }
    }


    // Defining a new simpler version of insertEventLog for use in CellTracker.
    // Please note, that in AMSICDDbAdapter (here) it is also used to backup DB,
    // in which case we can not use this simpler version!
    public void toEventLog(int DF_id, String DF_desc) {

        Long time = new Date().getTime();
        int lac = CellTracker.getCell().getLAC();
        int cid = CellTracker.getCell().getCellId();
        int psc = CellTracker.getCell().getPSC();
        double lat = CellTracker.getCell().getLat();
        double lon = CellTracker.getCell().getLon();
        int accuracy = (int) CellTracker.getCell().getAccuracy();

        // skip CID/LAC of "-1" (due to crappy API, Roaming or Air-Plane Mode)
        if(cid != -1 || lac != -1) {
            // Check if LAST entry is the same!
            String query = String.format(
                    "SELECT * from " + TABLE_EVENTLOG +
                            " WHERE _id=(SELECT max(_id) from " + TABLE_EVENTLOG +
                            ") AND cellid=%d AND LAC=%d AND PSC=%d AND DF_id=%d",
                    cid, lac, psc, DF_id);
            Cursor cursor = getWritableDatabase().rawQuery(query, null);

            // WARNING: By skipping duplicate events, we might be missing counts of Type-0 SMS etc.
            boolean insertData = true;
            if(cursor.getCount() > 0) {
                insertData = false;
            }
            cursor.close();

            if(insertData) {
                ContentValues eventLog = new ContentValues();

                eventLog.put("time", time);
                eventLog.put("LAC", lac);
                eventLog.put("cellid", cid);
                eventLog.put("PSC", psc);
                eventLog.put("lat", lat);
                eventLog.put("lon", lon);
                eventLog.put("accuracy", accuracy);
                eventLog.put("DF_id", DF_id);
                eventLog.put("DF_description", DF_desc);

                getWritableDatabase().insert(TABLE_EVENTLOG, null, eventLog);
                Log.i(TAG,
                      "ToEventLog(): Added new event: id=" + DF_id + " time=" + time + " cellid=" + cid);
            }
        }
    }

    /**
     * Inserts detected silent SMS data into "SmsData" table
     */
    public void insertSmsData(String time, String number, String smsc, String message,
                              String type, String clazz, int lac, int cellid, String tech,
                              double lat, double lon, int isRoaming) {

        ContentValues smsData = new ContentValues();

        smsData.put(COLUMN_time, time);
        smsData.put(COLUMN_number, number);
        smsData.put(COLUMN_smsc, smsc);
        smsData.put(COLUMN_message, message);
        smsData.put(COLUMN_type, type);
        smsData.put(COLUMN_class, clazz);
        smsData.put(COLUMN_lac, lac);
        smsData.put(COLUMN_cellId, cellid);
        smsData.put(COLUMN_technology, tech);
        smsData.put(COLUMN_lat, lat);
        smsData.put(COLUMN_lon, lon);
        smsData.put(COLUMN_isRoaming, isRoaming);

        try {
            // Check that SMS timestamp is not already in DB, then INSERT
            if(!isTimeStampInDB(Long.valueOf(time))) {
                getWritableDatabase().insert(TABLE_SILENT_SMS, null, smsData);
            }
        } catch(NumberFormatException nfe) {
            // what do?
        }
    }

    /**
     * This checks if a cell with a given CID already exists as imported cell.
     */
    public boolean openCellExists(int cellID) {
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_IMPORTS,
                new String[] {COLUMN_cellId},
                COLUMN_cellId + "=" + cellID, null, null, null, null, null);
        //@formatter:on

        return cursor.getCount() < 1;
    }

    public Cursor returnObservedCursor() {
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_OBSERVED,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_psc,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_technology,
                COLUMN_avg_signal,
                COLUMN_first_seen,
                COLUMN_last_seen,
                COLUMN_lat,
                COLUMN_lon},
                null, null, null, null, null);
        //@formatter:on
        return cursor;
    }


    public Cursor returnMeasuresCursor() {
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_MEASURES,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_dbm,
                COLUMN_time,
                COLUMN_tmsi,
                COLUMN_psc,
                COLUMN_technology,
                COLUMN_speed,
                COLUMN_accuracy},
                null, null, null, null, null);
        //@formatter:on
        return cursor;
    }

    public Cursor returnImportedCursor() {
        //@formatter:off
        Cursor cursor = getWritableDatabase().query(TABLE_IMPORTS,
            new String[] {
                COLUMN_cellId,
                COLUMN_lac,
                COLUMN_psc,
                COLUMN_mcc,
                COLUMN_mnc,
                COLUMN_technology,
                COLUMN_dbm,
                COLUMN_first_seen,
                COLUMN_last_seen,
                COLUMN_lat,
                COLUMN_lon,
                COLUMN_samples,
                COLUMN_avg_signal,
                COLUMN_source},
                null, null, null, null, null);
        //@formatter:on
        return cursor;
    }

}
