package org.badcoders.aimsicd;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.acra.ACRA;
import org.badcoders.aimsicd.activities.PrefActivity;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.adapters.DetailsPagerAdapter;
import org.badcoders.aimsicd.constants.DrawerMenu;
import org.badcoders.aimsicd.drawer.DrawerMenuActivityConfiguration;
import org.badcoders.aimsicd.drawer.NavDrawerItem;
import org.badcoders.aimsicd.fragments.AboutFragment;
import org.badcoders.aimsicd.fragments.AtCommandFragment;
import org.badcoders.aimsicd.fragments.CellCompareFragment;
import org.badcoders.aimsicd.fragments.DetailsContainerFragment;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.tasks.OpenCellIdKeyDownloadTask;
import org.badcoders.aimsicd.tasks.OpenCellIdUploadTask;
import org.badcoders.aimsicd.utils.Icon;
import org.badcoders.aimsicd.utils.NetworkUtil;

import java.util.List;

public class Aimsicd extends FragmentActivity {

    public static ProgressBar mProgressBar;

    private static final String TAG = "AIMSICD";

    private AimsicdService mAimsicdService;

    private DrawerMenuActivityConfiguration mNavConf;
    private SharedPreferences prefs;
    private SharedPreferences.OnSharedPreferenceChangeListener prefListener;
    private DrawerLayout mDrawerLayout;
    private ActionBar mActionBar;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private DatabaseAdapter mDbAdapter;
    private PowerManager.WakeLock wakeLock;

    private boolean mBound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getApp().setMainActivity(this);

        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        mNavConf = new DrawerMenuActivityConfiguration.Builder(this).build();

        setContentView(mNavConf.getMainLayout());

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mDrawerLayout = (DrawerLayout) findViewById(mNavConf.getDrawerLayoutId());
        mDrawerList = (ListView) findViewById(mNavConf.getLeftDrawerId());
        mActionBar = getActionBar();
        mTitle = mDrawerTitle = getTitle();

        PowerManager mgr = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                                   "MyWakeLock");
        wakeLock.acquire();

        mDrawerList.setAdapter(mNavConf.getBaseAdapter());

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                                                  R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                mActionBar.setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mActionBar.setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);

        prefs = getSharedPreferences(AimsicdService.SHARED_PREFERENCES_BASENAME, 0);

        prefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if(key.equals(getString(R.string.adv_user_root_pref_key))) {
                    smsDetection();
                }
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(prefListener);

        mDbAdapter = new DatabaseAdapter(this);

        checkFirstStart();
        startService();
        updateIcon();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mActionBar.setIcon(Icon.getIcon());
        mDrawerToggle.syncState();
        updateIcon();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateIcon();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unbind from the service
        if(mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    public void quit() {
        if(wakeLock.isHeld())
            wakeLock.release();

        try {
            if(mAimsicdService.isSilentSmsDetectionEnabled()) {
                mAimsicdService.stopSmsTracking();
            }
        } catch(Exception ee) {
            ACRA.getErrorReporter().handleSilentException(ee);
            Log.e(TAG, "Exception in smstracking module: " + ee.getMessage(), ee);
        }

        if(mAimsicdService != null)
            mAimsicdService.onDestroy();

        new DatabaseAdapter(getApplicationContext()).close();

        if(android.os.Build.VERSION.SDK_INT >= 21) {
            finishAndRemoveTask();
        } else {
            finish();
        }
    }

    private MyApplication getApp() {

        return (MyApplication) this.getApplication();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            mDrawerLayout.closeDrawer(mDrawerList);
            selectItem(position);
        }
    }

    private void updateIcon() {
        if(getActionBar() != null) {
            getActionBar().setIcon(Icon.getIcon());
        }
    }

    /**
     * Performs action based on clicked item
     *
     * @param position Position of clicked item from the drawer
     */
    private void selectItem(int position) {
        NavDrawerItem selectedItem = mNavConf.getNavItems().get(position);

        /**
         * This is a work-around for Issue 42601
         * https://code.google.com/p/android/issues/detail?id=42601
         *
         * The method getChildFragmentManager() does not clear up
         * when the Fragment is detached.
         */
        DetailsContainerFragment mDetailsFrag = new DetailsContainerFragment();

        // Create a new fragment
        switch(selectedItem.getId()) {
            case DrawerMenu.ID.MAIN.PHONE_SIM_DETAILS:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, mDetailsFrag).commit();
                mDetailsFrag.setCurrentPage(DetailsPagerAdapter.FragmentLocation.DEVICE_FRAGMENT.position);
                break;

            case DrawerMenu.ID.MAIN.NEIGHBORING_CELLS:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, mDetailsFrag).commit();
                mDetailsFrag.setCurrentPage(DetailsPagerAdapter.FragmentLocation.NEIGHBORING_CELLS.position);
                break;

            case DrawerMenu.ID.MAIN.DB_VIEWER:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, mDetailsFrag).commit();
                mDetailsFrag.setCurrentPage(DetailsPagerAdapter.FragmentLocation.DB_VIEWER_FRAGMENT.position);
                break;

            case DrawerMenu.ID.MAIN.ANTENNA_MAP_VIEW:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, mDetailsFrag).commit();
                mDetailsFrag.setCurrentPage(DetailsPagerAdapter.FragmentLocation.MAP_VIEW_FRAGMENT.position);
                break;

            case DrawerMenu.ID.MAIN.AT_COMMAND_INTERFACE:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new AtCommandFragment()).commit();
                break;

            case DrawerMenu.ID.APPLICATION.ABOUT:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new AboutFragment()).commit();
                break;

            case DrawerMenu.ID.APPLICATION.UPLOAD_LOCAL_BTS_DATA:
                if(NetworkUtil.isInternetConnectionAvailable(this)) {
                    new OpenCellIdUploadTask(this, mDbAdapter).execute();
                } else {
                    Toast.makeText(this, R.string.please_enable_network_connectivity,
                                   Toast.LENGTH_LONG).show();
                }
                break;

            case DrawerMenu.ID.TRACKING.TOGGLE_ATTACK_DETECTION:
                monitorCell();
                break;

            case DrawerMenu.ID.TRACKING.TOGGLE_CELL_TRACKING:
                changeCellTracking();
                break;

            case DrawerMenu.ID.TRACKING.TRACK_FEMTOCELL:
                trackFemtocell();
                break;

            case DrawerMenu.ID.SETTINGS.PREFERENCES:
                Intent intent = new Intent(this, PrefActivity.class);
                startActivity(intent);
                break;

            case DrawerMenu.ID.MAIN.VERIFY_CURRENT_CELL:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CellCompareFragment()).commit();
                break;

            case DrawerMenu.ID.APPLICATION.QUIT:
                quit();
                return;
        }

        mDrawerList.setItemChecked(position, true);

        if(this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        mActionBar.setTitle(mTitle);
    }

    /**
     * Service Connection to bind the activity to the service
     */
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mAimsicdService = ((AimsicdService.AimscidBinder) service).getService();
            mBound = true;

            // Check if tracking cell details check location services are still enabled
            if(mAimsicdService.isTrackingCell()) {
                mAimsicdService.checkLocationServices();
                mDbAdapter.insertBTS(mAimsicdService.getCell());
            }

            if(!mAimsicdService.isSilentSmsDetectionEnabled() &&
                       prefs.getBoolean(getString(R.string.adv_user_root_pref_key), false)) {
                smsDetection();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private void startService() {

        if(!mBound) {
            // Bind to LocalService
            Intent intent = new Intent(Aimsicd.this, AimsicdService.class);
            //Start Service before binding to keep it resident when activity is destroyed
            startService(intent);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

            // Display the Device Fragment as the Default View
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new DetailsContainerFragment()).commit();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        NavDrawerItem femtoTrackingItem = null;
        NavDrawerItem cellMonitoringItem = null;
        NavDrawerItem cellTrackingItem = null;

        List<NavDrawerItem> menuItems = mNavConf.getNavItems();
        for(NavDrawerItem lItem : menuItems) {
            if(lItem.getId() == DrawerMenu.ID.TRACKING.TOGGLE_ATTACK_DETECTION) {
                cellMonitoringItem = lItem;
            } else if(lItem.getId() == DrawerMenu.ID.TRACKING.TOGGLE_CELL_TRACKING) {
                cellTrackingItem = lItem;
            } else if(lItem.getId() == DrawerMenu.ID.TRACKING.TRACK_FEMTOCELL) {
                femtoTrackingItem = lItem;
            }
        }

        if(mBound) {
            if(cellMonitoringItem != null) {
                if(mAimsicdService.isMonitoringCell()) {
                    cellMonitoringItem.setIconId(R.drawable.track_cell);
                } else {
                    cellMonitoringItem.setIconId(R.drawable.untrack_cell);
                }
                mNavConf.getBaseAdapter().notifyDataSetChanged();
            }
            if(cellTrackingItem != null) {
                if(mAimsicdService.isTrackingCell()) {
                    cellTrackingItem.setIconId(R.drawable.track_cell);
                } else {
                    cellTrackingItem.setIconId(R.drawable.untrack_cell);
                }
                mNavConf.getBaseAdapter().notifyDataSetChanged();
            }

            if(femtoTrackingItem != null) {
                if(mAimsicdService.isTrackingFemtocell()) {
                    femtoTrackingItem.setIconId(R.drawable.ic_action_network_cell);
                } else {
                    femtoTrackingItem.setIconId(R.drawable.ic_action_network_cell_not_tracked);
                }
                mNavConf.getBaseAdapter().notifyDataSetChanged();
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void smsDetection() {

        boolean root_sms = prefs.getBoolean(getString(R.string.adv_user_root_pref_key), false);

        if(root_sms && !mAimsicdService.isSilentSmsDetectionEnabled()) {
            mAimsicdService.startSmsTracking();
            Toast.makeText(this, "SMS Detection Started", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "SMS Detection Thread Started");
        } else if(!root_sms && mAimsicdService.isSilentSmsDetectionEnabled()) {
            mAimsicdService.stopSmsTracking();
            Toast.makeText(this, "Sms Detection Stopped", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "SMS Detection Thread Stopped");
        }
    }

    public void changeCellTracking() {
        prefs.edit().putBoolean("tracking_state", !mAimsicdService.isTrackingCell()).commit();
        mAimsicdService.setCellTracking(!mAimsicdService.isTrackingCell());
        if(mAimsicdService.isTrackingCell()) {
            mDbAdapter.updateObserved(mAimsicdService.getCell());
        }
    }

    private void monitorCell() {
        mAimsicdService.setCellMonitoring(!mAimsicdService.isMonitoringCell());
        if(mAimsicdService.isMonitoringCell()) {
            mDbAdapter.updateObserved(mAimsicdService.getCell());
        }
    }

    /**
     * FemtoCell Detection (CDMA Phones ONLY) - Enable/Disable
     */
    private void trackFemtocell() {
        mAimsicdService.setTrackingFemtocell(!mAimsicdService.isTrackingFemtocell());
    }

    private void checkFirstStart() {
        if(prefs.getBoolean(getString(R.string.first_start), true)) {
            prefs.edit().putBoolean(getString(R.string.first_start), false).commit();
            if(NetworkUtil.isInternetConnectionAvailable(this)) {
                downloadOCIDKeyOnFirstStart();
            } else {
                Toast.makeText(this, getString(R.string.please_enable_network_connectivity),
                               Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void downloadOCIDKeyOnFirstStart() {
        // @formatter:off
        AlertDialog.Builder disclaimer = new AlertDialog.Builder(this)
           .setTitle(R.string.download_opencellid_key)
           .setMessage(R.string.first_start_download_opencell_id_key)
           .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
                   if(NetworkUtil.isInternetConnectionAvailable(Aimsicd.this)) {
                       ProgressDialog pd = new ProgressDialog(Aimsicd.this);
                       pd.setMessage(getString(R.string.getting_ocid_key));
                       pd.show();
                       OpenCellIdKeyDownloadTask ocikdt = new OpenCellIdKeyDownloadTask(Aimsicd.this, prefs, pd);
                       ocikdt.execute();
                   } else {
                       Toast.makeText(Aimsicd.this, R.string.please_enable_network_connectivity,
                                      Toast.LENGTH_LONG).show();
                   }
               }
           })
           .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
               }
           });
        // @formatter:on

        AlertDialog disclaimerAlert = disclaimer.create();
        disclaimerAlert.show();
    }
}
