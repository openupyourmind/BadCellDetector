package org.badcoders.aimsicd.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.tasks.OpenCellIdLookUpTask;
import org.badcoders.aimsicd.tasks.RadioCellHelper;
import org.badcoders.aimsicd.tasks.RadioCellLookupTask;
import org.badcoders.aimsicd.utils.Formatter;
import org.badcoders.aimsicd.utils.Helpers;

import java.util.Date;

public class CellCompareFragment extends Fragment {

    public static final String TAG = CellCompareFragment.class.toString();

    private Context mContext;

    private View view;
    private TextView ccSignalTextView;
    private GraphView graph;
    private TelephonyManager mTelManager;

    protected Cell cell;
    private CellStrengthReceiver csl;
    private AimsicdService mAimsicdService;
    private boolean mBound = false;

    /**
     * Service Connection to bind the activity to the service
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mAimsicdService = ((AimsicdService.AimscidBinder) service).getService();
            cell = mAimsicdService.getCell();
            setupUi();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
        if(isAdded() && mBound == false) {
            Intent intent = new Intent(mContext, AimsicdService.class);
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(mBound == false) {
            Intent intent = new Intent(mContext, AimsicdService.class);
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mBound == false) {
            // Bind to LocalService
            Intent intent = new Intent(mContext, AimsicdService.class);
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDestroy() {
        // Unbind from the service
        if(mBound) {
            mContext.unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.view = inflater.inflate(R.layout.cell_compare_fragment, container, false);
        setupUi();
        return view;
    }

    private void setupUi() {
        if(view != null && mAimsicdService != null && isAdded()) {
            ((TextView) view.findViewById(R.id.cc_cell_id)).setText(
                    String.valueOf(cell.getCellId()));
            ((TextView) view.findViewById(R.id.cc_lac)).setText(String.valueOf(cell.getLAC()));
            ((TextView) view.findViewById(R.id.cc_mcc)).setText(String.valueOf(cell.getMCC()));
            ((TextView) view.findViewById(R.id.cc_mnc)).setText(String.valueOf(cell.getMNC()));
            ((TextView) view.findViewById(R.id.cc_psc)).setText(String.valueOf(cell.getPSC()));
            ((TextView) view.findViewById(R.id.cc_rat)).setText(cell.getTechnology());
            ccSignalTextView = (TextView) view.findViewById(R.id.cc_signal);

            // @formatter:off
            if(cell.getLat() != 0.0)
                ((TextView) view.findViewById(R.id.cc_lat)).setText(Formatter.formatLocation(cell.getLat()));

            if(cell.getLon() != 0.0)
                ((TextView) view.findViewById(R.id.cc_lon)).setText(Formatter.formatLocation(cell.getLon()));

            if(cell.getFirstSeen() != 0)
                ((TextView) view.findViewById(R.id.cc_first_seen)).setText(Formatter.formatDate(cell.getFirstSeen()));
            else
                ((TextView) view.findViewById(R.id.cc_first_seen)).setText(Formatter.formatDate(new Date()));

            if(cell.getLastSeen() != 0)
                ((TextView) view.findViewById(R.id.cc_last_seen)).setText(Formatter.formatDate(cell.getLastSeen()));
            else
                ((TextView) view.findViewById(R.id.cc_last_seen)).setText(Formatter.formatDate(new Date()));

            ((TextView) view.findViewById(R.id.cc_samples)).setText(String.valueOf(cell.getSamples()));
            // @formatter:on

            graph = (GraphView) view.findViewById(R.id.graph);
            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setYAxisBoundsManual(true);

            graph.getGridLabelRenderer().setVerticalAxisTitle(this.getString(R.string.RSSI_dbm));
            graph.getGridLabelRenderer().setHighlightZeroLines(true);
            graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
            graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
            graph.getGridLabelRenderer().setNumVerticalLabels(3);

            graph.getViewport().setMaxX(60);
            graph.getViewport().setMinX(0);

            graph.getViewport().setMaxY(-50);
            graph.getViewport().setMinY(-115);
            sendRequests();

            csl = new CellStrengthReceiver();
            mTelManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            mTelManager.listen(csl, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        }
    }

    private void sendRequests() {
        if(isVisible()) {
            new OpenCellIdLookup(mContext).execute(cell); //just to speed it up a bit
            new RadioCellLookup(mContext).execute(cell);
        }
    }

    private class RadioCellLookup extends RadioCellLookupTask {

        private Context mContext;

        public RadioCellLookup(Context context) {
            this.mContext = context;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(result == 200) {
                JsonParser jsonParser = new JsonParser();
                JsonObject jo = (JsonObject) jsonParser.parse(body);
                JsonArray jsonArr = jo.getAsJsonArray("features");
                if(jsonArr.size() == 0) {
                    Toast.makeText(mContext,
                                   mContext.getString(R.string.cell_not_found_in_radiocell_db),
                                   Toast.LENGTH_SHORT).show();
                    ((TextView) view.findViewById(R.id.rc_lac)).setText(
                            mContext.getString(R.string.not_found));
                    ((TextView) view.findViewById(R.id.rc_lac)).setTextColor(0xffff0000);
                    return;
                }
                // @formatter:off
                Cell loadedCell = RadioCellHelper.getCellFromJson(jsonArr.get(0).getAsJsonObject());
                ((TextView) view.findViewById(R.id.rc_lac)).setText(String.valueOf(loadedCell.getLAC()));
                ((TextView) view.findViewById(R.id.rc_mcc)).setText(String.valueOf(loadedCell.getMCC()));
                ((TextView) view.findViewById(R.id.rc_mnc)).setText(String.valueOf(loadedCell.getMNC()));
                ((TextView) view.findViewById(R.id.rc_psc)).setText(String.valueOf(loadedCell.getPSC()));
                ((TextView) view.findViewById(R.id.rc_signal)).setText(String.valueOf(loadedCell.getDBM()));
                ((TextView) view.findViewById(R.id.rc_rat)).setText(loadedCell.getTechnology());
                ((TextView) view.findViewById(R.id.rc_lat)).setText(Formatter.formatLocation(loadedCell.getLat()));
                ((TextView) view.findViewById(R.id.rc_lon)).setText(Formatter.formatLocation(loadedCell.getLon()));
                ((TextView) view.findViewById(R.id.rc_first_seen)).setText(Formatter.formatDate(loadedCell.getFirstSeen()));
                ((TextView) view.findViewById(R.id.rc_last_seen)).setText(Formatter.formatDate(loadedCell.getLastSeen()));
                ((TextView) view.findViewById(R.id.rc_samples)).setText(String.valueOf(loadedCell.getSamples()));

                //mark unmatching values in red
                // because context.getColor was introduces in API 23, I used
                // 0xffff0000 which corresponds to light_red in color file
                if(loadedCell.getLAC() != cell.getLAC())
                    ((TextView) view.findViewById(R.id.rc_lac)).setTextColor(0xffff0000);
                if(loadedCell.getMCC() != cell.getMCC())
                    ((TextView) view.findViewById(R.id.rc_mcc)).setTextColor(0xffff0000);
                if(loadedCell.getMNC() != cell.getMNC())
                    ((TextView) view.findViewById(R.id.rc_mnc)).setTextColor(0xffff0000);
                if(loadedCell.getPSC() != cell.getPSC())
                    ((TextView) view.findViewById(R.id.rc_psc)).setTextColor(0xffff0000);
                if(!loadedCell.getTechnology().equals(cell.getTechnology()))
                    ((TextView) view.findViewById(R.id.rc_rat)).setTextColor(0xffff0000);
                // @formatter:on

            } else if(result == -1) {
                Toast.makeText(mContext, mContext.getString(
                        R.string.unable_to_request_cell_data_without_cell_id),
                               Toast.LENGTH_SHORT).show();
            } else if(result == -1010101) {
                Toast.makeText(mContext, mContext.getString(R.string.unable_to_build_request),
                               Toast.LENGTH_LONG).show();
            } else if(result == -1000) {
                Toast.makeText(mContext, mContext.getString( //blocked by firewall or DNS problem
                                                             R.string.unknown_problem_connecting_to_online_resource_encountered),
                               Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, mContext.getString(
                        R.string.unknown_problem_connecting_to_online_resource_encountered),
                               Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class OpenCellIdLookup extends OpenCellIdLookUpTask {

        private Context mContext;

        public OpenCellIdLookup(Context context) {
            this.mContext = context;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(result == 200) {
                if(cells == null && cells.isEmpty()) {
                    Toast.makeText(mContext,
                                   mContext.getString(R.string.cell_not_found_in_radiocell_db),
                                   Toast.LENGTH_SHORT).show();
                    ((TextView) view.findViewById(R.id.ocid_lac)).setText(
                            mContext.getString(R.string.not_found));
                    ((TextView) view.findViewById(R.id.ocid_lac)).setTextColor(0xffff0000);
                    return;
                }
                // @formatter:off
                Cell loadedCell = cells.get(0);
                ((TextView) view.findViewById(R.id.ocid_lac)).setText(String.valueOf(loadedCell.getLAC()));
                ((TextView) view.findViewById(R.id.ocid_mcc)).setText(String.valueOf(loadedCell.getMCC()));
                ((TextView) view.findViewById(R.id.ocid_mnc)).setText(String.valueOf(loadedCell.getMNC()));
                ((TextView) view.findViewById(R.id.ocid_psc)).setText(String.valueOf(loadedCell.getPSC()));
                ((TextView) view.findViewById(R.id.ocid_signal)).setText(String.valueOf(loadedCell.getDBM()));
                ((TextView) view.findViewById(R.id.ocid_rat)).setText(loadedCell.getTechnology());
                ((TextView) view.findViewById(R.id.ocid_lat)).setText(Formatter.formatLocation(loadedCell.getLat()));
                ((TextView) view.findViewById(R.id.ocid_lon)).setText(Formatter.formatLocation(loadedCell.getLon()));

                if(loadedCell.getFirstSeen() != 0)
                    ((TextView) view.findViewById(R.id.ocid_first_seen)).setText(Formatter.formatDate(loadedCell.getFirstSeen()));

                if(loadedCell.getLastSeen() != 0)
                    ((TextView) view.findViewById(R.id.ocid_last_seen)).setText(Formatter.formatDate(loadedCell.getLastSeen()));

                ((TextView) view.findViewById(R.id.ocid_samples)).setText(String.valueOf(loadedCell.getSamples()));

                //mark unmatching values in red
                // because context.getColor was introduces in API 23, I used
                // 0xffff0000 which corresponds to light_red in color file
                if(loadedCell.getLAC() != cell.getLAC())
                    ((TextView) view.findViewById(R.id.ocid_lac)).setTextColor(0xffff0000);
                if(loadedCell.getMCC() != cell.getMCC())
                    ((TextView) view.findViewById(R.id.ocid_mcc)).setTextColor(0xffff0000);
                if(loadedCell.getMNC() != cell.getMNC())
                    ((TextView) view.findViewById(R.id.ocid_mnc)).setTextColor(0xffff0000);
                if(loadedCell.getPSC() != cell.getPSC())
                    ((TextView) view.findViewById(R.id.ocid_psc)).setTextColor(0xffff0000);
                if(!loadedCell.getTechnology().equals(cell.getTechnology()))
                    ((TextView) view.findViewById(R.id.ocid_rat)).setTextColor(0xffff0000);
                // @formatter:on

            } else if(result == -1) {
                Toast.makeText(mContext, mContext.getString(
                        R.string.unable_to_request_cell_data_without_cell_id),
                               Toast.LENGTH_SHORT).show();
            } else if(result == -1010101) {
                Toast.makeText(mContext, mContext.getString(R.string.unable_to_build_request),
                               Toast.LENGTH_LONG).show();
            } else if(result == -1000) {
                Toast.makeText(mContext, mContext.getString( //blocked by firewall or DNS problem
                                                             R.string.unknown_problem_connecting_to_online_resource_encountered),
                               Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, mContext.getString(
                        R.string.unknown_problem_connecting_to_online_resource_encountered),
                               Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class CellStrengthReceiver extends PhoneStateListener {

        private int mCurrentLevel,
                mPreviousLevel = -100;

        private double graph2LastXValue = 0d;
        private LineGraphSeries mMeasurements;
        private PointsGraphSeries highlight;

        CellStrengthReceiver() {
            super();
        }

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);

            mCurrentLevel = Helpers.getDbm(signalStrength.getGsmSignalStrength());
            cell.setDBM(mCurrentLevel);

            if(graph != null) {
                if(mMeasurements == null) {
                    mMeasurements = new LineGraphSeries<>();
                    mMeasurements.setColor(Color.GREEN);
                    graph.addSeries(mMeasurements);
                }

                if(highlight == null) {
                    highlight = new PointsGraphSeries<>();
                    highlight.setSize(5f);
                    highlight.setColor(Color.RED);
                    graph.addSeries(highlight);
                }
                // a this point highlight should never be null
                if(Math.abs(mPreviousLevel - mCurrentLevel) > 9) {
                    highlight.appendData(new DataPoint(graph2LastXValue, mCurrentLevel), true, 60);
                }
            }

            if(mCurrentLevel != 85) {
                mMeasurements.appendData(new DataPoint(graph2LastXValue, mCurrentLevel), true, 60);
            } else {
                mMeasurements.appendData(new DataPoint(graph2LastXValue, 0), true, 60);
            }

            ccSignalTextView.setText(String.valueOf(mCurrentLevel));
            mPreviousLevel = mCurrentLevel;
            graph2LastXValue += 1d;
        }
    }

}
