package org.badcoders.aimsicd.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import org.badcoders.aimsicd.R;

public class PrefFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

}
