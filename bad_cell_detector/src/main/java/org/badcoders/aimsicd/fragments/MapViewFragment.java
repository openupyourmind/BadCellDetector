/*
Copyright (c) 2015-* by agilob

ISC License - The acceptable one

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD
TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/
package org.badcoders.aimsicd.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.map.CellTowerGridMarkerClusterer;
import org.badcoders.aimsicd.map.CellTowerMarker;
import org.badcoders.aimsicd.service.AimsicdService;
import org.badcoders.aimsicd.tasks.OpenCellIdDownloadTask;
import org.badcoders.aimsicd.tasks.RadioCellDownloadTask;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.utils.GeoLocation;
import org.badcoders.aimsicd.utils.Helpers;
import org.badcoders.aimsicd.utils.NetworkUtil;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.List;

public class MapViewFragment extends Fragment {

    private static final String TAG = "MapViewFragment";
    private static final int INITIAL_ZOOM = 15;

    private Context mContext;
    private MapView mMap;
    private CellTowerGridMarkerClusterer mCellTowerGridMarkerClusterer;
    private DatabaseAdapter mDbHelper;
    private TelephonyManager tm;
    private AimsicdService mAimsicdService;
    private DatabaseAdapter mDbAdapter;
    private GeoPoint loc;
    private boolean mBound;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.setHasOptionsMenu(true);

        this.mContext = context;
        this.mDbHelper = new DatabaseAdapter(mContext);
        this.tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        this.mDbAdapter = new DatabaseAdapter(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Bind to LocalService
        Intent intent = new Intent(mContext, AimsicdService.class);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        View view = inflater.inflate(R.layout.map, container, false);
        setupMap(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Bind to LocalService
        if(!mBound) {
            Intent intent = new Intent(mContext, AimsicdService.class);
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unbind from the service
        if(mBound) {
            mContext.unbindService(mConnection);
            mBound = false;
        }
    }

    private void setupMap(View view) {

        mMap = (MapView) view.findViewById(R.id.mapview);
        mMap.getTileProvider().createTileCache();

        mMap.setBuiltInZoomControls(true);
        mMap.setMultiTouchControls(true);
        mMap.setMinZoomLevel(3);
        mMap.setMaxZoomLevel(21);

        // Sets cluster pin color
        mCellTowerGridMarkerClusterer = new CellTowerGridMarkerClusterer(mContext);
        mCellTowerGridMarkerClusterer.setIcon(((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.ic_map_pin_orange)).getBitmap());

        GpsMyLocationProvider gpsMyLocationProvider = new GpsMyLocationProvider(mContext);
        // the minimum distance for location updates
        gpsMyLocationProvider.setLocationUpdateMinDistance(50);
        // minimum time interval for location updates
        gpsMyLocationProvider.setLocationUpdateMinTime(10000);

        loadEntries();

        ScaleBarOverlay myScaleBarOverlay = new ScaleBarOverlay(mMap);

        MyLocationNewOverlay mMyLocationOverlay = new MyLocationNewOverlay(mContext,
                                                                           gpsMyLocationProvider,
                                                                           mMap);
        mMyLocationOverlay.setDrawAccuracyEnabled(true);

        mMyLocationOverlay.enableMyLocation();

        mMap.getOverlays().add(mCellTowerGridMarkerClusterer);
        mMap.getOverlays().add(mMyLocationOverlay);
        mMap.getOverlays().add(myScaleBarOverlay);
    }

    private void loadEntries() {

        new AsyncTask<Void, Void, GeoPoint>() {
            @Override
            protected GeoPoint doInBackground(Void... voids) {

                mCellTowerGridMarkerClusterer.getItems().clear();

                //New function only gets bts from DBe_import by sim network
                loadMarkersByNetwork();

                //mCellTowerGridMarkerClusterer.add(ovm);

                GeoPoint ret = new GeoPoint(51, 0);//put map in Greenwich

                // plot neighbouring cells
                while(mAimsicdService == null)
                    try {
                        Thread.sleep(100);
                    } catch(InterruptedException e) {
                        Log.w(TAG, "thread interrupted", e);
                    }
                List<Cell> nc = mAimsicdService.getCellTracker().updateNeighbouringCells();

                for(Cell cell : nc) {
                    loc = new GeoPoint(cell.getLat(), cell.getLon());
                    CellTowerMarker ovm =
                            new CellTowerMarker(mContext, mMap,
                                                getString(R.string.cell_id) + cell.getCellId(),
                                                loc, cell);

                    // The pin of other BTS
                    ovm.setIcon(getResources().getDrawable(R.drawable.ic_map_pin_orange));
                    mCellTowerGridMarkerClusterer.add(ovm);
                }

                return ret;
            }

            /**
             * We need a manual way to add our own location in case:
             * a) GPS is jammed or not working
             * b) WiFi location is not used
             * c) Default MCC is too far off
             */
            @Override
            protected void onPostExecute(GeoPoint defaultLoc) {

                if(loc != null &&
                           (Double.doubleToRawLongBits(loc.getLatitude()) != 0
                                    && Double.doubleToRawLongBits(loc.getLongitude()) != 0)) {
                    mMap.getController().setZoom(INITIAL_ZOOM);
                    mMap.getController().setCenter(new GeoPoint(loc.getLatitude(),
                                                                loc.getLongitude()));
                } else {
                    if(mBound) {
                        // Try and find last known location and zoom there
                        GeoLocation lastLoc = mAimsicdService.lastKnownLocation();
                        if(lastLoc != null) {
                            Log.d(TAG, "Using last known location");
                            loc = new GeoPoint(lastLoc.getLatitudeInDegrees(),
                                               lastLoc.getLongitudeInDegrees());
                        } else {
                            Log.d(TAG, "Using default location");
                            //Use MCC to move camera to an approximate location near Countries Capital
                            loc = defaultLoc;
                        }
                        mMap.getController().setZoom(INITIAL_ZOOM);
                        mMap.getController().animateTo(new GeoPoint(loc.getLatitude(),
                                                                    loc.getLongitude()));
                    }
                }
                if(mCellTowerGridMarkerClusterer != null) {
                    //Drawing markers of cell tower immediately as possible
                    mCellTowerGridMarkerClusterer.invalidate();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadMarkersByNetwork() {
        String networkOperator = tm.getNetworkOperator();
        int mmc = Helpers.getMMCFromString(networkOperator);
        int mnc = Helpers.getMNCFromString(networkOperator);

        mCellTowerGridMarkerClusterer.addAll(mDbHelper.getImportedCellTowerMarkers(mMap, mmc, mnc));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.map_viewer_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean isOpenCellIdServiceEnabled = prefs.getBoolean("remote_service_opencellid_enabled",
                                                              false);
        boolean isRadioCellServiceEnabled = prefs.getBoolean("remote_service_radiocell_enabled",
                                                             true);

        if(isOpenCellIdServiceEnabled == false && isRadioCellServiceEnabled == false) {
            Toast.makeText(mContext,
                           mContext.getString(R.string.you_must_enable_at_least_one_remote_service),
                           Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean hasNetworkConnection = NetworkUtil.isInternetConnectionAvailable(mContext);

        if(item.getItemId() == R.id.download_cell_data) {
            if(hasNetworkConnection == false) {// if no internet connection leave method
                Toast.makeText(mContext, R.string.please_enable_network_connectivity,
                               Toast.LENGTH_SHORT).show();
                return false;
            }

            Toast.makeText(mContext,
                           getString(R.string.contacting_online_resources_for_data),
                           Toast.LENGTH_SHORT).show();

            Cell cell = mAimsicdService.getCell();
            cell.setLat(mMap.getMapCenter().getLatitude());
            cell.setLon(mMap.getMapCenter().getLongitude());

            if(isOpenCellIdServiceEnabled) {
                Log.d(TAG, "Using OCID data on user request");
                OpenCellIdDownloadTask ocidt = new OpenCellIdDownloadTask(mContext,
                                                                          mDbAdapter);
                ocidt.execute(cell);
            }
            if(isRadioCellServiceEnabled) {
                Log.d(TAG, "Using RadioCell data on user request");
                RadioCellDownloadTask rcdt = new RadioCellDownloadTask(mContext,
                                                                       mDbAdapter);
                rcdt.execute(cell);
            }

            //if any of them is enabled...
            return (isOpenCellIdServiceEnabled || isRadioCellServiceEnabled);
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Service Connection to bind the activity to the service <p> This seem to setup the connection
     * and animates the map window movement to the last known location.
     */
    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mAimsicdService = ((AimsicdService.AimscidBinder) service).getService();
            mBound = true;

            // setup map
            GeoLocation lastKnown = mAimsicdService.lastKnownLocation();
            if(lastKnown != null) {
                mMap.getController().setZoom(INITIAL_ZOOM); // Initial Zoom level
                mMap.getController().animateTo(new GeoPoint(lastKnown.getLatitudeInDegrees(),
                                                            lastKnown.getLongitudeInDegrees()));
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

}
