package org.badcoders.aimsicd.activities;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import org.badcoders.aimsicd.R;

public class RemoteServicesPreference extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadFragment();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadFragment();
    }

    private void loadFragment() {
        GeneralPreferenceFragment generalFragment = new GeneralPreferenceFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, generalFragment);
        fragmentTransaction.commit();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_remote_services);
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                    this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(
                    this);
            super.onPause();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            final String ocid_pref_name = "remote_service_opencellid_enabled";
            final String radiocell_pref_name = "remote_service_radiocell_enabled";
            if(ocid_pref_name.equals(key)) {
                CheckBoxPreference lp = (CheckBoxPreference) findPreference(ocid_pref_name);
                if(lp.isChecked()) {
                    Intent intent = new Intent(getActivity(), OpenCellIdActivity.class);
                    startActivity(intent);
                }
                findPreference("pref_request_ocid_pref_key").setEnabled(lp.isChecked());
            } else if(radiocell_pref_name.equals(key)) {
                CheckBoxPreference lp = (CheckBoxPreference) findPreference(radiocell_pref_name);
                findPreference("auto_upload_to_radiocell").setEnabled(lp.isChecked());
            }
        }
    }
}
