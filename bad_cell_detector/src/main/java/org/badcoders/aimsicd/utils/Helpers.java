/**
 * Copyright (C) 2013  Louis Teboul    <louisteboul@gmail.com> <p> This program is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License, or (at your option)
 * any later version. <p> This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License for more details. <p> You should have
 * received a copy of the GNU General Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

package org.badcoders.aimsicd.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Helpers {

    private static final String TAG = "Helpers";
    private static final int CHARS_PER_LINE = 34;

    /**
     * Return a String List representing response from invokeOemRilRequestRaw
     *
     * @param aob
     *         Byte array response from invokeOemRilRequestRaw
     */
    public static List<String> unpackByteListOfStrings(byte aob[]) {

        if(aob.length == 0) {
            Log.v(TAG, "invokeOemRilRequestRaw: byte-list response Length = 0");
            return Collections.emptyList();
        }
        int lines = aob.length / CHARS_PER_LINE;
        String[] display = new String[lines];

        for(int i = 0; i < lines; i++) {
            int offset, byteCount;
            offset = i * CHARS_PER_LINE + 2;
            byteCount = 0;

            if(offset + byteCount >= aob.length) {
                Log.e(TAG, "Unexpected EOF");
                break;
            }
            while(aob[offset + byteCount] != 0 && (byteCount < CHARS_PER_LINE)) {
                byteCount += 1;
                if(offset + byteCount >= aob.length) {
                    Log.e(TAG, "Unexpected EOF");
                    break;
                }
            }
            display[i] = new String(aob, offset, byteCount).trim();
        }
        int newLength = display.length;
        while(newLength > 0 && TextUtils.isEmpty(display[newLength - 1])) {
            newLength -= 1;
        }
        return Arrays.asList(Arrays.copyOf(display, newLength));
    }

    public static int getMMCFromString(String networkOperator) {
        if(networkOperator.isEmpty())
            return -1;
        if(networkOperator.length() > 2) {
            return Integer.valueOf(networkOperator.substring(0, 3));
        } else {
            return Integer.valueOf(networkOperator.substring(0, networkOperator.length()));
        }
    }

    public static int getMNCFromString(String networkOperator) {
        if(networkOperator.isEmpty())
            return -1;
        if(networkOperator.length() <= 5) {
            return Integer.valueOf(networkOperator.substring(3, 5));
        }
        return -1;
    }

    /**
     * Network Type
     *
     * @return string representing device Network Type
     */
    public static String getNetworkTypeName(int netType) {
        switch(netType) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1xRTT";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "eHRPD";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EVDO_0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EVDO_A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "EVDO_B";
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPA+";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "iDEN";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
            default:
                return "NA";
        }
    }

    public static String getSystemProp(Context context, String prop, String def) {

        String result = null;
        try {
            result = SystemPropertiesReflection.get(context, prop);
        } catch(IllegalArgumentException iae) {
            Log.e(TAG, "Failed to get system property: " + prop, iae);
        }
        return result == null ? def : result;
    }

    public static int getDbm(int dbm) {
        if(dbm == 99) { // 99 is invalid
            return 85; // 99 * 2 - 113 = 85
        } else if(dbm > 0) {
            return (dbm * 2) - 113;
        }
        return dbm;
    }
}
