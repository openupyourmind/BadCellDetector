package org.badcoders.aimsicd.utils.atcmd;

import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Description:    ...
 * <p/>
 * Issues:     [ ] This probably won't work well with two clients! I don't know what happens
 * if your RIL currently uses the same AT interface.
 * [ ] TODO track down SIGPIPE (apparently in "cat /dev/smd7") on uncaught exception?
 * The stack barf in logcat is bugging me, but I spent some time trying
 * to figure it out and can't.
 * <p/>
 * Notes:
 * QCom: /dev/smd7, possibly other SMD devices. On 2 devices I've checked,
 * smd7 is owned by bluetooth:bluetooth, so that could be something to sniff for if
 * it's not always smd7.
 * <p/>
 * More common is that modem AT CoP is on:  /dev/smd0
 * while the Bluetooth modem (which also us AT CoP is on: /dev/smd7
 * <p/>
 * ChangeLog:
 */
public abstract class AtCommandTerminal {

    private static final String TAG = "AtCommandTerminal";

    public abstract void send(String s, Message message);

    public abstract void dispose();

    /**
     * @return
     * @throws UnsupportedOperationException if no instance can be made
     */
    public static AtCommandTerminal factory() throws UnsupportedOperationException {
        AtCommandTerminal term = null;

        File smdFile = new File("/dev/smd7");
        if (smdFile.exists()) {
            try {
                term = new TtyPrivFile(smdFile.getAbsolutePath());
            } catch (IOException e) {
                Log.e(TAG, "IOException in constructor", e);
                // fall through
            }
        }

        if (term == null) {
            throw new UnsupportedOperationException("unable to find AT command terminal");
        }

        return term;
    }

}
