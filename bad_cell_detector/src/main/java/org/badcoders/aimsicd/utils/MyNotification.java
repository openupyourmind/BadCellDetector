package org.badcoders.aimsicd.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import org.badcoders.aimsicd.Aimsicd;
import org.badcoders.aimsicd.MyApplication;
import org.badcoders.aimsicd.R;
import org.badcoders.aimsicd.service.AimsicdService;

public class MyNotification extends Notification {

    public static final int NOTIFICATION_ID = 1234567;

    private Context mContext;
    private static int trackingIcon;
    private static SharedPreferences prefs;
    private static boolean isTracking;
    private static Intent notificationIntent;
    private static PendingIntent intent;
    private static PendingIntent stopServiceIntent;
    private static PendingIntent changeTrackingIntent;
    private static NotificationManager notificationManager;

    public MyNotification(Context context, String title, String summary) {
        super();
        this.mContext = context;

        if(prefs == null)
            prefs = context.getSharedPreferences(AimsicdService.SHARED_PREFERENCES_BASENAME, 0);

        if(notificationManager == null)
            notificationManager = (NotificationManager)
                                          mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if(notificationIntent == null) {
            notificationIntent = new Intent(mContext, Aimsicd.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_FROM_BACKGROUND
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        if(intent == null)
            intent = PendingIntent.getActivity(mContext, NOTIFICATION_ID, notificationIntent,
                                               PendingIntent.FLAG_UPDATE_CURRENT);

        if(stopServiceIntent == null)
            stopServiceIntent = PendingIntent.getBroadcast(mContext, NOTIFICATION_ID,
                                                           new Intent(MyApplication.NOTIFICATION_STOP),
                                                           PendingIntent.FLAG_UPDATE_CURRENT);

        if(changeTrackingIntent == null)
            changeTrackingIntent = PendingIntent.getBroadcast(mContext, NOTIFICATION_ID,
                                                              new Intent(MyApplication.TRACKING_CHANGE),
                                                              PendingIntent.FLAG_UPDATE_CURRENT);

        isTracking = prefs.getBoolean(context.getString(R.string.tracking_state), true);

        if(isTracking) {
            trackingIcon = R.drawable.untrack_cell;
        } else {
            trackingIcon = R.drawable.track_cell;
        }

        // @formatter:off
        Notification notification = new NotificationCompat.Builder(context)
                    .setSmallIcon(Icon.notificationIcon())
                    .setTicker(title)
                    .setContentText(summary)
                    .setContentTitle(context.getResources().getString(R.string.main_app_name))
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .setWhen(0) // hides timestamp
                    .setContentIntent(intent)
                    .addAction(android.R.drawable.ic_menu_close_clear_cancel,
                               context.getString(R.string.quit),
                               stopServiceIntent)
                    .addAction(trackingIcon,
                               context.getString(R.string.toggle_cell_tracking),
                               changeTrackingIntent)
                    .build();
        // @formatter:on
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

}
