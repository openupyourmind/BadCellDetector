package org.badcoders.aimsicd.utils;

import org.badcoders.aimsicd.R;

/**
 * Class that holds and returns the correct icon based on requested icon format and
 * current system status.
 */
public class Icon {

    /*
     * Returns a icon of the Type $t, what kind of icon is returned is decided
     * from what the current status is.
     */
    public static int getIcon() {
        switch(Status.getStatus()) {
            case IDLE:
                return R.drawable.white_idle;
            case NORMAL:
                return R.drawable.white_ok;
            case MEDIUM:
                return R.drawable.white_medium;
            case HIGH:
                return R.drawable.white_high;
            case ALARM:
                return R.drawable.white_danger;
            case DANGEROUS:
                return R.drawable.white_danger;
            case RUN:
                return R.drawable.white_skull;
            default:
                return R.drawable.white_idle;
        }
    }

    public static int notificationIcon() {
        return R.drawable.white_idle;
    }
}
