package org.badcoders.aimsicd.enums;

import org.badcoders.aimsicd.R;

import java.util.Arrays;
import java.util.List;

public enum StatesDbViewer {

     UNIQUE_BTS_DATA(R.string.unique_bts_data, 0),
     BTS_MEASUREMENTS(R.string.bts_measurements, 1),
     IMPORTED_OCID_DATA(R.string.imported_cell_data, 2),
     SILENT_SMS(R.string.silent_sms, 3),
     EVENT_LOG(R.string.eventlog, 4),
     DETECTION_STRINGS(R.string.detection_strings, 5);
     // DETECTION_FLAGS(R.string.detection_flags)

    public final int statementValue;
    public final int value;

    StatesDbViewer(int mStatementValue, int value) {
        this.statementValue = mStatementValue;
        this.value = value;
    }

    public static List<StatesDbViewer> getStates() {
        return Arrays.asList(values());
    }

}
