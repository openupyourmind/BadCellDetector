package org.badcoders.badcelldetector;

import android.content.Context;
import android.test.InstrumentationTestCase;

import org.badcoders.aimsicd.adapters.DatabaseAdapter;
import org.badcoders.aimsicd.model.Cell;
import org.badcoders.aimsicd.tasks.RadioCellDownloadTask;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class RadioCellDownloadTest extends InstrumentationTestCase {

    private Context mContext;
    private DatabaseAdapter dbAdapter;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mContext = getInstrumentation().getTargetContext();
        dbAdapter = new DatabaseAdapter(getInstrumentation().getTargetContext());
    }

    public void testImportingFromRadioCell() throws Throwable {

        final CountDownLatch signal = new CountDownLatch(1);

        class RadioCellDownloadTestTask extends RadioCellDownloadTask {
            public RadioCellDownloadTestTask(Context context, DatabaseAdapter adapter) {
                super(context, adapter);
            }
        }

        final RadioCellDownloadTestTask radioCellDownloadTask = new RadioCellDownloadTestTask(mContext,
                                                                                              dbAdapter);
        final Cell location = new Cell();
        location.setLat(51.455313);
        location.setLon(-2.591902);

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                radioCellDownloadTask.execute(location);
            }
        });

        signal.await(10, TimeUnit.SECONDS);

        assertTrue("No networks imported!,",
                   dbAdapter.getImportedCellsByNetwork(234, 10).size() >= 0);
    }

}
